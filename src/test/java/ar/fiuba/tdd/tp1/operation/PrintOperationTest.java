package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.exceptions.InsufficientArgumentsOnPrint;
import ar.fiuba.tdd.tp1.parsers.operation.PrintOperation;

import org.junit.Test;

import static org.junit.Assert.*;

public class PrintOperationTest {

    @Test
    public void printBasicTest() {
        PrintOperation printer = new PrintOperation();
        String result = printer.operate("hola $1, como $2?", "Nico", "estas").toString();
        assertEquals("hola Nico, como estas?", result);
    }

    @Test
    public void printMinArgumentsTest() {
        PrintOperation printer = new PrintOperation();
        String result = printer.operate("hola Nico como estas?").toString();
        assertEquals("hola Nico como estas?", result);
    }
    
    @Test(expected = InsufficientArgumentsOnPrint.class)
    public void notEnoughParametersTest() {
        PrintOperation printer = new PrintOperation();
        printer.operate("hola $1, como $2?", "Nico").toString();
    }
    
    @Test
    public void literalValuesOnStringTest(){
        PrintOperation printer = new PrintOperation();
        String result = printer.operate("$1, tu cuenta es de &$44", "Martin").toString();
        assertEquals("Martin, tu cuenta es de $44", result);
    }
    
    @Test
    public void literalValuesAndComplexReferencesOnStringTest(){
        PrintOperation printer = new PrintOperation();
        String result = printer.operate("buenos dias, tu cuenta es &$44 queres abonar "
        		+ "en $1 o en $2? tenes una deuda de &$5 tambien", "efectivo", "tarjeta").toString();
        assertEquals("buenos dias, tu cuenta es $44 queres abonar en "
        		+ "efectivo o en tarjeta? tenes una deuda de $5 tambien", result);
    }
    
}
