package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.InvalidFormulaException;
import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.SpreadSheetApplication;
import ar.fiuba.tdd.tp1.model.formulas.Formula;
import ar.fiuba.tdd.tp1.parsers.CollaborativeParser;
import ar.fiuba.tdd.tp1.parsers.CollaborativeParserOrderer;
import ar.fiuba.tdd.tp1.util.DateBinaryOperation;
import ar.fiuba.tdd.tp1.util.DateComparator;
import ar.fiuba.tdd.tp1.util.DoubleBinaryOperation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.*;

public class CollaborativeParserTest {

	private static final double DELTA = 0.0001;
	private CollaborativeParser collaborativeParser;
	private SpreadSheetApplication app;

	@Before
	public void setUp() throws Exception {
		app = new SpreadSheetApplication();
		app.addBook("book1");
		app.addBook("tecnicas");
		app.addSheet("book1", "sheet1");
		app.addSheet("book1", "sheet2");
		app.addSheet("book1", "sheet3");
		Book book = app.getBook("book1");
		collaborativeParser = new CollaborativeParserOrderer().getCollaborativeParser(book);
	}

	@After
	public void tearDown() throws Exception {
		app.restart();
	}

	@Test
	public void testParseValueFormula() {

		try {
			assertEquals("Value 2 parse Error.", 2, collaborativeParser.parse("2").eval());
		} catch (Exception e) {
			assert false;
		}
	}

	@Test
	public void testParseOtherValueFormula() {
		try {
			assertEquals("Value 5 parse Error.", 5, collaborativeParser.parse("5").eval());
		} catch (Exception e) {
			assert false;
		}
	}

	@Test(expected = Exception.class)
	public void testParseInvalidExpressionToFormula() throws Exception {
		collaborativeParser.parse("= iuzdsfgios + 068j").eval();
	}

	@Test
	public void testParseValueAddition() throws Exception {
		double expected = (double) 3;
		Formula parsed = collaborativeParser.parse("= 2 + 1");
		Object actual = parsed.eval();
		assertEquals("2 + 1 Parse OK", expected, actual);
	}

	@Test
	public void testParseValueSubstraction() throws Exception {
		assertEquals("8 - 2 Parse OK", (double) 6, collaborativeParser.parse("= 8 -2").eval());
	}

	@Test
	public void testParseCellReference() throws Exception {
		app.setFormulaOf("book1", "sheet3", "A6", "9");
		app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6");
		assertEquals(9, app.getValueOf("book1", "sheet1", "C3"), DELTA);
	}

	@Test
	public void testParseCellReferenceAdditionWhitValue() throws Exception {
		app.setFormulaOf("book1", "sheet3", "A6", "9");
		app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6 + 2");
		assertEquals(11, app.getValueOf("book1", "sheet1", "C3"), DELTA);
	}

	@Test
	public void testParseCellReferenceSubstractionWhitValue() throws Exception {
		app.setFormulaOf("book1", "sheet3", "A6", "9");
		app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6 - 2");
		assertEquals(7, app.getValueOf("book1", "sheet1", "C3"), DELTA);
	}

	@Test
	public void testParseCellReferenceAddition() throws Exception {
		app.setFormulaOf("book1", "sheet3", "A6", "9");
		app.setFormulaOf("book1", "sheet1", "C2", "18");
		app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6 + !sheet1.C2");
		assertEquals(27, app.getValueOf("book1", "sheet1", "C3"), DELTA);
	}

	@Test
	public void testParseCellReferenceSubstraction() throws Exception {
		app.setFormulaOf("book1", "sheet3", "A6", "9");
		app.setFormulaOf("book1", "sheet1", "C2", "18");
		app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6 - !sheet1.C2");
		assertEquals(-9, app.getValueOf("book1", "sheet1", "C3"), DELTA);
	}

	@Test
	public void testParseCellComplexOperation() throws Exception {
		app.setFormulaOf("book1", "sheet3", "A6", "1");
		app.setFormulaOf("book1", "sheet1", "C2", "2");
		app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6 + 4 - !sheet1.C2 + 1");
		assertEquals(4, app.getValueOf("book1", "sheet1", "C3"), DELTA);
	}

	// TESTS AGREGADOS POR EL
	// GRUPO05------------------------------------------------------

	@Test
	public void testParseValueAdditionAndSustraction() throws Exception {
		double expected = (double) 1;
		Formula parsed = collaborativeParser.parse("= 2 + 1 - 2");
		Object actual = parsed.eval();
		assertEquals("2 + 1 - 2 Parse OK", expected, actual);
	}

	@Test
	public void testParseValueAdditionAndSustraction2() throws Exception {
		double expected = (double) 3;
		Formula parsed = collaborativeParser.parse("= 2 - 1 + 2");
		Object actual = parsed.eval();
		assertEquals("2 - 1 + 2 Parse OK", expected, actual);
	}

	@Test
	public void testParseValuePower() throws Exception {
		double expected = (double) 8;
		Formula parsed = collaborativeParser.parse("= 2 ^ 3");
		Object actual = parsed.eval();
		assertEquals("2 ^ 3 Parse OK", expected, actual);
	}

	@Test
	public void testParseValuePowerWithVariable() throws Exception {
		app.setFormulaOf("book1", "sheet3", "A6", "2");
		app.setFormulaOf("book1", "sheet1", "C2", "2");
		app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6 ^ !sheet1.C2 ");
		assertEquals(4, app.getValueOf("book1", "sheet1", "C3"), DELTA);
	}

	@Test
	public void testParseValuePower2() throws Exception {
		double expected = (double) 5;
		Formula parsed = collaborativeParser.parse("= 2 ^ 2 + 1");
		Object actual = parsed.eval();
		assertEquals("2 ^ 2 + 1 Parse OK", expected, actual);
	}

	@Test
	public void testParseValuePowerWithVariable2() throws Exception {
		app.setFormulaOf("book1", "sheet3", "A6", "2");
		app.setFormulaOf("book1", "sheet1", "C2", "2");
		app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6 ^ !sheet1.C2 + 1");
		assertEquals(5, app.getValueOf("book1", "sheet1", "C3"), DELTA);
	}

	@Test
	public void testParseValuePower3() throws Exception {
		double expected = (double) 3;
		Formula parsed = collaborativeParser.parse("= 2 ^ 2 - 1");
		Object actual = parsed.eval();
		assertEquals("2 ^ 2 - 1 Parse OK", expected, actual);
	}

	@Test
	public void testParseValuePowerWithVariable3() throws Exception {
		app.setFormulaOf("book1", "sheet3", "A6", "2");
		app.setFormulaOf("book1", "sheet1", "C2", "2");
		app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6 ^ !sheet1.C2 - 1");
		assertEquals(3, app.getValueOf("book1", "sheet1", "C3"), DELTA);
	}

	@Test
	public void testParseValuePower4() throws Exception {
		double expected = (double) 5;
		Formula parsed = collaborativeParser.parse("= 1 + 2 ^ 2");
		Object actual = parsed.eval();
		assertEquals("1 + 2 ^ 2 Parse OK", expected, actual);
	}

	@Test
	public void testParseValuePowerWithVariable4() throws Exception {
		app.setFormulaOf("book1", "sheet3", "A6", "2");
		app.setFormulaOf("book1", "sheet1", "C2", "2");
		app.setFormulaOf("book1", "sheet1", "C3", "= 1 + !sheet3.A6 ^ !sheet1.C2 ");
		assertEquals(5, app.getValueOf("book1", "sheet1", "C3"), DELTA);
	}

	@Test
	public void testParseValuePower5() throws Exception {
		double expected = (double) -3;
		Formula parsed = collaborativeParser.parse("= 1 - 2 ^ 2");
		Object actual = parsed.eval();
		assertEquals("1 - 2 ^ 2 Parse OK", expected, actual);
	}

	@Test
	public void testParseValuePowerWithVariable5() throws Exception {
		app.setFormulaOf("book1", "sheet3", "A6", "2");
		app.setFormulaOf("book1", "sheet1", "C2", "2");
		app.setFormulaOf("book1", "sheet1", "C3", "= 1 - !sheet3.A6 ^ !sheet1.C2 ");
		assertEquals(-3, app.getValueOf("book1", "sheet1", "C3"), DELTA);
	}

	@Test
	public void testParseValuePowerWithVariable6() throws Exception {
		app.setFormulaOf("book1", "sheet3", "A6", "2");
		app.setFormulaOf("book1", "sheet1", "C2", "2");
		app.setFormulaOf("book1", "sheet1", "C3", "= 1 - !sheet3.A6 ^ 2 - !sheet1.C2 ");
		assertEquals(-5, app.getValueOf("book1", "sheet1", "C3"), DELTA);
	}

	@Test
	public void testParseTodayWithNoOtherFormulas() throws Exception {
		String bookName = "book1";
		String sheetName = "sheet3";
		String cellName = "A6";
		app.setFormulaOf(bookName, sheetName, cellName, "=TODAY()");
		Date cellContent = (Date) app.getCellContent(bookName, sheetName, cellName);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date today = new GregorianCalendar().getTime();
		assertEquals(simpleDateFormat.format(today), simpleDateFormat.format(cellContent));
	}

	@Test
	public void testConcat() throws Exception {
		String bookName = "book1";
		String sheetName = "sheet3";
		app.setFormulaOf(bookName, sheetName, "A1", "Hola");
		app.setFormulaOf(bookName, sheetName, "A2", " como estas");
		app.setFormulaOf(bookName, sheetName, "A3", "=CONCAT(A1,A2, todo bien)");

		Object cellContent = app.getCellContent(bookName, sheetName, "A3");
		assertEquals("Hola como estas todo bien", cellContent.toString());
	}

	@Test
	public void testMax() throws Exception {
		app.setFormulaOf("tecnicas", "default", "A1", "10 ");
		app.setFormulaOf("tecnicas", "default", "A2", "80.234");
		app.setFormulaOf("tecnicas", "default", "A3", "10");
		app.setFormulaOf("tecnicas", "default", "A4", "= MAX(A1:A3)");

		Object cellContent = app.getCellContent("tecnicas", "default", "A4");
		Double doubleCellContent = ((Number) cellContent).doubleValue();
		assertEquals(80.234, doubleCellContent, DELTA);
	}

	@Test
	public void testMax2() throws Exception {
		app.setFormulaOf("tecnicas", "default", "A1", "10 ");
		app.setFormulaOf("tecnicas", "default", "A2", "80.234");
		app.setFormulaOf("tecnicas", "default", "A4", "= MAX(A1:A3)");
		app.setFormulaOf("tecnicas", "default", "A3", "10");

		Object cellContent = app.getCellContent("tecnicas", "default", "A4");
		Double doubleCellContent = ((Number) cellContent).doubleValue();
		assertEquals(80.234, doubleCellContent, DELTA);
	}

	@Test
	public void testMax3() throws Exception {
		app.setFormulaOf("tecnicas", "default", "A1", "10 ");
		app.setFormulaOf("tecnicas", "default", "A2", "80.234");
		app.setFormulaOf("tecnicas", "default", "A3", "100000");
		app.setFormulaOf("tecnicas", "default", "A4", "= MAX(A1:A3)");
		app.setFormulaOf("tecnicas", "default", "A3", "10");

		Object cellContent = app.getCellContent("tecnicas", "default", "A4");
		Double doubleCellContent = ((Number) cellContent).doubleValue();
		assertEquals(80.234, doubleCellContent, DELTA);
	}

	@Test
	public void testMax4() throws Exception {
		app.setFormulaOf("tecnicas", "default", "A1", "10 ");
		app.setFormulaOf("tecnicas", "default", "A2", "80.234");
		app.setFormulaOf("tecnicas", "default", "A3", "100000");
		app.setFormulaOf("tecnicas", "default", "A4", "= MAX(A3:A1)");
		app.setFormulaOf("tecnicas", "default", "A3", "10");

		Object cellContent = app.getCellContent("tecnicas", "default", "A4");
		Double doubleCellContent = ((Number) cellContent).doubleValue();
		assertEquals(80.234, doubleCellContent, DELTA);
	}

	@Test
	public void testMixMaxAndMin() throws Exception {
		app.setFormulaOf("tecnicas", "default", "A1", "10 ");
		app.setFormulaOf("tecnicas", "default", "A2", "80.234");
		app.setFormulaOf("tecnicas", "default", "A3", "10");
		app.setFormulaOf("tecnicas", "default", "B1", "15 ");
		app.setFormulaOf("tecnicas", "default", "B2", "70.6786");
		app.setFormulaOf("tecnicas", "default", "B3", "20.23");
		app.setFormulaOf("tecnicas", "default", "A4", "= MAX(A1:B3) - MIN(A1:B3)");

		Object cellContent = app.getCellContent("tecnicas", "default", "A4");
		Double doubleCellContent = ((Number) cellContent).doubleValue();
		assertEquals(70.234, doubleCellContent, DELTA);
	}

	@Test
	public void testLeftWithVariable() throws Exception {
		app.setFormulaOf("tecnicas", "default", "A1", "Hola");
		app.setFormulaOf("tecnicas", "default", "A4", "=LEFT(A1,3)");

		Object cellContent = app.getCellContent("tecnicas", "default", "A4");
		assertEquals("Hol", cellContent.toString());
	}

	@Test
	public void testLeftWithLiteral() throws Exception {
		app.setFormulaOf("tecnicas", "default", "A4", "=LEFT(Hola,3)");

		Object cellContent = app.getCellContent("tecnicas", "default", "A4");
		assertEquals("Hol", cellContent.toString());
	}

	@Test
	public void testRightWithVariable() throws Exception {
		app.setFormulaOf("tecnicas", "default", "A1", "Hola");
		app.setFormulaOf("tecnicas", "default", "A4", "=RIGHT(A1,3)");

		Object cellContent = app.getCellContent("tecnicas", "default", "A4");
		assertEquals("ola", cellContent.toString());
	}

	@Test
	public void testRightWithLiteral() throws Exception {
		app.setFormulaOf("tecnicas", "default", "A4", "=RIGHT(Hola,3)");

		Object cellContent = app.getCellContent("tecnicas", "default", "A4");
		assertEquals("ola", cellContent.toString());
	}

	@Test
	public void testLeftWithVariableExceeded() throws Exception {
		app.setFormulaOf("tecnicas", "default", "A1", "Esta es una frase");
		app.setFormulaOf("tecnicas", "default", "A4", "=LEFT(A1,10)");

		Object cellContent = app.getCellContent("tecnicas", "default", "A4");
		assertEquals("Esta es un", cellContent.toString());
	}

	@Test
	public void testLeftWithLiteralExceeded() throws Exception {
		app.setFormulaOf("tecnicas", "default", "A4", "=LEFT(Esta es una frase,34)");

		Object cellContent = app.getCellContent("tecnicas", "default", "A4");
		assertEquals("Esta es una frase", cellContent.toString());
	}

	@Test
	public void testRightWithVariableExceeded() throws Exception {
		app.setFormulaOf("tecnicas", "default", "A1", "Esta es una frase");
		app.setFormulaOf("tecnicas", "default", "A4", "=RIGHT(A1,17)");

		Object cellContent = app.getCellContent("tecnicas", "default", "A4");
		assertEquals("Esta es una frase", cellContent.toString());
	}

	@Test
	public void testRightWithLiteralExceeded() throws Exception {
		app.setFormulaOf("tecnicas", "default", "A4", "=RIGHT(Esta es una frase,16)");

		Object cellContent = app.getCellContent("tecnicas", "default", "A4");
		assertEquals("sta es una frase", cellContent.toString());
	}

	@Test
	public void testAddDateWithMillis() throws Exception {
		long oneDay = 1000 * 60 * 60 * 24;
		app.setFormulaOf("tecnicas", "default", "A1", "=2015/10/21 + " + oneDay);
		Date cellContent = (Date) app.getCellContent("tecnicas", "default", "A1");
		assertEquals("2015/10/22", new SimpleDateFormat("yyyy/MM/dd").format(cellContent));
	}

	@Test
	public void testSubtractDateWithMillis() throws Exception {
		long oneDay = 1000 * 60 * 60 * 24;
		app.setFormulaOf("tecnicas", "default", "A1", "=2015/10/21 - " + oneDay);
		Date cellContent = (Date) app.getCellContent("tecnicas", "default", "A1");
		assertEquals("2015/10/20", new SimpleDateFormat("yyyy/MM/dd").format(cellContent));
	}

	@Test
	public void testAddTwoTodays() throws Exception {
		app.setFormulaOf("tecnicas", "default", "A1", "=TODAY() + TODAY()");
		Date cellContent = (Date) app.getCellContent("tecnicas", "default", "A1");

		GregorianCalendar first = new GregorianCalendar();
		GregorianCalendar second = new GregorianCalendar();
		Date expected = DateBinaryOperation.create().withOperation(DoubleBinaryOperation::addition).operate(first, second);

		assertTrue(DateComparator.compare().yearMonthDay(expected, cellContent));
	}

	@Test
	public void testGetYesterday() throws Exception {
		long oneDay = 1000 * 60 * 60 * 24;
		app.setFormulaOf("tecnicas", "default", "A1", "=TODAY() - " + oneDay);
		Date cellContent = (Date) app.getCellContent("tecnicas", "default", "A1");

		GregorianCalendar first = new GregorianCalendar();
		Date expected = DateBinaryOperation.create().withOperation(DoubleBinaryOperation::subtraction).operate(first, oneDay);

		assertTrue(DateComparator.compare().yearMonthDay(expected, cellContent));
	}

	@Test
	public void testPrint() throws Exception {
		String bookName = "tecnicas";
		String sheetName = "default";
		String cellName = "A1";
		app.setFormulaOf(bookName, sheetName, cellName, "= PRINT(hola $1 como $2?,Nico,estas)");
		String cellContent = app.getCellContent(bookName, sheetName, cellName).toString();
		assertEquals("hola Nico como estas?", cellContent);
	}

	@Test
	public void testPrintMinArgs() throws Exception {
		String bookName = "tecnicas";
		String sheetName = "default";
		String cellName = "A1";
		app.setFormulaOf(bookName, sheetName, cellName, "=PRINT(hola como estas?)");
		String cellContent = app.getCellContent(bookName, sheetName, cellName).toString();
		assertEquals("hola como estas?", cellContent);
	}

	@Test(expected = InvalidFormulaException.class)
	public void testInvalidPrint() throws Exception {
		String bookName = "tecnicas";
		String sheetName = "default";
		String cellName = "A1";
		app.setFormulaOf(bookName, sheetName, cellName, "=PRINT(hola $1 como $2?,Nico)");
		String cellContent = app.getCellContent(bookName, sheetName, cellName).toString();
		assertEquals("hola Nico como estas?", cellContent);
	}

	@Test
	public void testInvalidPrintAsString() throws Exception {
		String bookName = "tecnicas";
		String sheetName = "default";
		String cellName = "A1";
		app.setFormulaOf(bookName, sheetName, cellName, "=PRINT(hola $1 como $2?,Nico)");
		String cellContent = app.getCellContentAsString(bookName, sheetName, cellName);
		assertTrue(cellContent.isEmpty());
	}
}