package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.HashMappedSheet;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Estos son tests que fuimos necesitando hacer del paquete Model.
 * 
 * @author Galli
 *
 */
public class ModelTest {

    @Test
    public void setRangeInSheet() {
        HashMappedSheet sheet = new HashMappedSheet();
        sheet.setRange("rango", "A1:A4");
        assertEquals(sheet.getRange("rango"), "A1:A4");
    }

    @Test
    public void setAndRemoveRangeInSheet() {
        HashMappedSheet sheet = new HashMappedSheet();
        sheet.setRange("rango", "A1:A4");
        sheet.removeRange("rango");
        assertEquals(sheet.getRange("rango"), "");
    }

    @Test
    public void setRangeInBook() {
        Book book = new Book();
        book.setRange("rango", "A1:A4");
        assertEquals(book.getRange("default", "rango"), "A1:A4");
    }

    @Test
    public void setAndRemoveRangeInBook() {
        Book book = new Book();
        book.setRange("rango", "A1:A4");
        book.removeRange("rango");
        assertEquals(book.getRange("default", "rango"), "");
    }

}
