package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellReferenceException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.model.formulas.CellContent;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Leandro on 19-Oct-15. :)
 */
public class FormatterTest {

    private CellContent cellContent;

    @Before
    public void setUp() {
        cellContent = new CellContent();

    }

    @Ignore("No tienen bien los formatos")
    @Test
    public void testFormatDecimalWithTwoDecimalDigits() throws InvalidFormatException, InvalidCellReferenceException {
        cellContent.setContent(null, "9.32");
        cellContent.setContentType("Number");
        cellContent.setFormatter("decimal", "2");
        assertEquals("FormatDecimalWithTwoDecimalDigits OK", "9.32", cellContent.getValueAsString());
    }

    @Test
    public void testFormatDecimalWithoutDecimalDigits() throws InvalidFormatException, InvalidCellReferenceException {
        cellContent.setContent(null, "37.87");
        cellContent.setContentType("Number");
        cellContent.setFormatter("decimal", "0");
        assertEquals("FormatDecimalWithTwoDecimalDigits OK", "38", cellContent.getValueAsString());
    }

    @Ignore("No tienen bien los formatos")
    @Test
    public void testFormatDecimalWithThreeDecimalDigits() throws InvalidFormatException, InvalidCellReferenceException {
        cellContent.setContent(null, "18.537");
        cellContent.setContentType("Number");
        cellContent.setFormatter("decimal", "3");
        assertEquals("FormatDecimalWithThreeDecimalDigits OK", "18.537", cellContent.getValueAsString());
    }

    @Ignore("No tienen bien los formatos")
    @Test
    public void testFormatDecimalWithMoreDecimalDigitsThanOriginal() throws InvalidFormatException, InvalidCellReferenceException {
        cellContent.setContent(null, "18.537");
        cellContent.setContentType("Number");
        cellContent.setFormatter("decimal", "9");
        assertEquals("FormatDecimalWithThreeDecimalDigits OK", "18.537000000", cellContent.getValueAsString());
    }

    @Ignore("No tienen bien los formatos")
    @Test
    public void testFormatMoneyWithTwoDecimalDigits() throws InvalidFormatException, InvalidCellReferenceException {
        cellContent.setContent(null, "18.537");
        cellContent.setContentType("Currency");
        cellContent.setFormatter("symbol", "$");
        cellContent.setFormatter("decimal", "2");
        assertEquals("FormatMoneyWithTwoDecimalDigit OK", "$ 18.54", cellContent.getValueAsString());
    }

    @Test
    public void testFormatMoneyWithoutDecimalDigits() throws InvalidFormatException, InvalidCellReferenceException {
        cellContent.setContent(null, "18.537");
        cellContent.setContentType("Currency");
        cellContent.setFormatter("symbol", "@");
        cellContent.setFormatter("decimal", "0");
        assertEquals("FormatMoneyWithoutDecimalDigits OK", "@ 19", cellContent.getValueAsString());
    }

}
