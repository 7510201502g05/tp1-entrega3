package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.InsufficientArgumentsOnPrint;
import ar.fiuba.tdd.tp1.exceptions.NoSheetsException;
import ar.fiuba.tdd.tp1.model.SpreadSheetApplication;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by ale on 10/3/15.
 */
public class SpreadSheetAppTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetApplication app;

    @Before
    public void setUp() throws Exception {
        app = new SpreadSheetApplication();
        app.addBook("book1");
        app.addSheet("book1", "sheet1");
    }

    @After
    public void tearDown() throws Exception {
        app.restart();
    }

    /*------------------------------------------------------------------------------
           TESTEO DEL SET FORMULA Y SU COMANDO
    -------------------------------------------------------------------------------*/

    @Test
    public void testObtainSimpleValue() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "11");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 11);
    }

    @Test
    public void testUndoFormulaSet() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "2");
        app.setFormulaOf("book1", "sheet1", "A1", "8");

        assertEquals(8, (int) app.getValueOf("book1", "sheet1", "A1"));
        app.undo();
        assertEquals(2, (int) app.getValueOf("book1", "sheet1", "A1"));
    }

    @Test
    public void testDoubleUndoFormulaSet() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "6");
        app.setFormulaOf("book1", "sheet1", "A1", "9");
        app.setFormulaOf("book1", "sheet1", "A1", "15");

        assertEquals(15, (int) app.getValueOf("book1", "sheet1", "A1"));
        app.undo();
        assertEquals(9, (int) app.getValueOf("book1", "sheet1", "A1"));
        app.undo();
        assertEquals(6, (int) app.getValueOf("book1", "sheet1", "A1"));
    }

    @Test
    public void testRedoFormulaSet() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "6");
        app.setFormulaOf("book1", "sheet1", "A1", "9");

        assertEquals(9, (int) app.getValueOf("book1", "sheet1", "A1"));
        app.undo();
        assertEquals("Falla el undo", 6, (int) app.getValueOf("book1", "sheet1", "A1"));
        app.redo();
        assertEquals("Falla el redo", 9, (int) app.getValueOf("book1", "sheet1", "A1"));
    }

    /*------------------------------------------------------------------------------
            TESTEO DEL NEW SHEET Y SU COMANDO
     -------------------------------------------------------------------------------*/

    @Test
    public void testAddSheet() throws Exception {
        try {
            app.setFormulaOf("book1", "sheet1", "H7", "9");
        } catch (NoSheetsException e) {
            fail("No pude crear otra hoja");
        }

        assertEquals("No pude crear otra hoja", 9, (int) app.getValueOf("book1", "sheet1", "H7"));
    }

    @Test(expected = NoSheetsException.class)
    public void testUndoAddSheet() throws Exception {
        app.addSheet("book1", "sheet2");
        try {
            app.setFormulaOf("book1", "sheet2", "H7", "9");
        } catch (NoSheetsException e) {
            fail("No pude crear otra hoja");
        }

        assertEquals("No pude crear otra hoja", 9, (int) app.getValueOf("book1", "sheet2", "H7"));

        app.undo(); // undo del setFormulaOf(2, "H7", "9");
        app.undo(); // undo del addSheet()

        app.getValueOf("book1", "sheet2", "H7");// Deberia lanzar NoSheetException
    }

    @Test
    public void testRedoAddSheet() throws Exception {
        try {
            app.setFormulaOf("book1", "sheet1", "H7", "9");
        } catch (NoSheetsException e) {
            fail("No pude crear otra hoja");
        }
        assertEquals("No pude crear otra hoja", 9, (int) app.getValueOf("book1", "sheet1", "H7"));

        app.undo(); // undo del setFormulaOf(2, "H7", "9");
        app.undo(); // undo del addSheet()

        app.redo(); // se vuelve a crear la sheet
        app.setFormulaOf("book1", "sheet1", "H7", "65");
        assertEquals("El redo de addSheet NO funciona", 65,
                (int) app.getValueOf("book1", "sheet1", "H7"));
    }

    /*------------------------------------------------------------------------------
           TESTEO DEL REMOVE SHEET Y SU COMANDO
    -------------------------------------------------------------------------------*/

    @Test(expected = NoSheetsException.class)
    public void testRemoveFirstSheet() throws Exception {
        app.removeSheet("book1", "sheet1");
        app.setFormulaOf("book1", "sheet1", "H7", "9"); // Deberia lanzar NoSheetException
    }

    @Test(expected = NoSheetsException.class)
    public void testRemoveSomeSheet() throws Exception {
        app.addSheet("book1", "sheet2");
        app.addSheet("book1", "sheet3");
        app.removeSheet("book1", "sheet3");
        // Se corren todas para la izquierda
        app.setFormulaOf("book1", "sheet3", "H7", "9"); // Deberia lanzar NoSheetException
    }

    @Test
    public void testUndoRemoveSheet() throws Exception {
        app.setFormulaOf("book1", "sheet1", "H7", "9");
        app.removeSheet("book1", "sheet1");
        app.undo();
        // La MISMA hoja que saco vuelve a entrar
        assertEquals("El undo de removeSheet NO funciona", 9,
                (int) app.getValueOf("book1", "sheet1", "H7"));
    }

    @Test(expected = NoSheetsException.class)
    public void testRedoRemoveSheet() throws Exception {
        app.setFormulaOf("book1", "sheet1", "H7", "9");
        app.removeSheet("book1", "sheet1");
        app.undo(); // Vuelve a dejar la hoja
        app.redo(); // Vuelva a SACAR la hoja
        app.setFormulaOf("book1", "sheet1", "H7", "9"); // Deberia lanzar NoSheetException
    }

    /*------------------------------------------------------------------------------
           TESTEO DE ARITMETICA DE CELDAS
    -------------------------------------------------------------------------------*/

    @Test
    public void testObtainResultSum() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "= 2 + 2");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 2 + 2);
    }

    @Test
    public void testObtainResultSubtract() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "= 2 - 2");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 2 - 2);
    }

    @Test
    public void testResultSumWithCell() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "A1", "= !sheet1.B2 + 2");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 5 + 2);
    }

    @Test
    public void testResultSubtractWithCell() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "A1", "= !sheet1.B2 - 2");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 5 - 2);
    }

    @Test
    public void testResultMultiSum() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "= 2 + 2 + 4 + 5");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 2 + 2 + 4 + 5);
    }

    @Test
    public void testResultMultiSubtract() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "= 2 - 2 - 4 - 5");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 2 - 2 - 4 - 5);
    }

    @Test
    public void testResultMultiSumWithCell() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "10");
        app.setFormulaOf("book1", "sheet1", "A1", "= !sheet1.B2 + 2 + 2 + 4 + 5");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 10 + 2 + 2 + 4 + 5);
    }

    @Test
    public void testResultMultiSubtractWithCell() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "10");
        app.setFormulaOf("book1", "sheet1", "A1", "= !sheet1.B2 - 2 - 2 - 4 - 5");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 10 - 2 - 2 - 4 - 5);
    }

    @Test
    public void testResultMultiCellSum() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "10");
        app.setFormulaOf("book1", "sheet1", "C10", "5");
        app.setFormulaOf("book1", "sheet1", "D3", "1");
        app.setFormulaOf("book1", "sheet1", "G5", "-3");
        app.setFormulaOf("book1", "sheet1", "A1",
                "= !sheet1.B2 + !sheet1.C10 + !sheet1.D3 + !sheet1.G5");
        assertEquals((10 + 5 + 1 + (-3)), (int) app.getValueOf("book1", "sheet1", "A1"));
    }

    @Test
    public void testResultMultiCellSubtract() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "10");
        app.setFormulaOf("book1", "sheet1", "C10", "5");
        app.setFormulaOf("book1", "sheet1", "D3", "1");
        app.setFormulaOf("book1", "sheet1", "G5", "-3");
        app.setFormulaOf("book1", "sheet1", "A1",
                "= !sheet1.B2 - !sheet1.C10 - !sheet1.D3 - !sheet1.G5");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 10 - 5 - 1 - (-3));
    }

    @Test
    public void testResultMultiOperand() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "= 33 + 5 - 100 + 8 - 10");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 33 + 5 - 100 + 8 - 10);
    }

    @Test
    public void testResultMultiOperandCell() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "10");
        app.setFormulaOf("book1", "sheet1", "C10", "5");
        app.setFormulaOf("book1", "sheet1", "D3", "1");
        app.setFormulaOf("book1", "sheet1", "G5", "-3");
        app.setFormulaOf("book1", "sheet1", "A1",
                "= !sheet1.B2 - !sheet1.C10 + !sheet1.D3 - !sheet1.G5");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 10 - 5 + 1 - (-3));
    }

    @Test
    public void testResultInterSheetCell() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "10");
        app.setFormulaOf("book1", "sheet1", "C10", "5");
        app.addSheet("book1", "sheet2");
        app.setFormulaOf("book1", "sheet2", "D3", "1");
        app.addSheet("book1", "sheet3");
        app.setFormulaOf("book1", "sheet3", "G5", "-3");
        app.setFormulaOf("book1", "sheet1", "A1",
                "= !sheet1.B2 - !sheet1.C10 + !sheet2.D3 - !sheet3.G5");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 10 - 5 + 1 - (-3));
    }

    @Test
    public void testResultInterSheetCellComposite() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "10");
        app.setFormulaOf("book1", "sheet1", "C10", "= 5 + !sheet1.B2");
        app.addSheet("book1", "sheet2");
        app.setFormulaOf("book1", "sheet2", "D3", "= 1 - !sheet1.C10");
        app.addSheet("book1", "sheet3");
        app.setFormulaOf("book1", "sheet3", "G5", "= -3 + !sheet2.D3");
        app.setFormulaOf("book1", "sheet1", "A1", "= !sheet3.G5 + !sheet1.B2");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), (-3) + (1 - (5 + 10)) + 10);
    }

    // Mas tests... del grupo 5

    @Test
    public void testResultSumWithCellOfOtherSheet() throws Exception {
        app.setFormulaOf("book1", "default", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "A1", "= !default.B2 + 2");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 5 + 2);
    }

    @Test
    public void testMax() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setFormulaOf("book1", "sheet1", "A1", "= MAX(B2:B4)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 7);
    }

    @Test
    public void setRangeInSpreadSheet() throws Exception {
        app.setRange("book1", "sheet1", "rango", "A1:A5");
        assertEquals(app.getRange("book1", "sheet1", "rango"), "A1:A5");
    }

    @Test
    public void setRangeInSpreadSheetAndUndo() throws Exception {
        app.setRange("book1", "sheet1", "rango", "A1:A5");
        app.undo();
        assertEquals(app.getRange("book1", "sheet1", "rango"), "");
    }

    @Test
    public void setAndRemoveRangeInSpreadSheet() throws Exception {
        app.setRange("book1", "sheet1", "rango", "A1:A5");
        app.removeRange("book1", "sheet1", "rango");
        assertEquals(app.getRange("book1", "sheet1", "rango"), "");
    }

    @Test
    public void setAndRemoveRangeInSpreadSheetAndUndoAndRedo() throws Exception {
        app.setRange("book1", "sheet1", "rango", "A1:A5");
        app.removeRange("book1", "sheet1", "rango");
        app.undo();
        assertEquals(app.getRange("book1", "sheet1", "rango"), "A1:A5");
        app.redo();
        assertEquals(app.getRange("book1", "sheet1", "rango"), "");
    }

    @Test
    public void testMax2() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setFormulaOf("book1", "sheet1", "C4", "9");
        app.setFormulaOf("book1", "sheet1", "A1", "= MAX(B4:C1)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 9);
    }

    @Test
    public void testMax3() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setRange("book1", "sheet1", "rango", "B2:B4");
        app.setFormulaOf("book1", "sheet1", "A1", "= MAX(B2:B4)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 7);
    }

    @Test
    public void testMaxWithRange() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setRange("book1", "sheet1", "rango", "B2:B4");
        app.setFormulaOf("book1", "sheet1", "A1", "= MAX(!sheet1.rango)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 7);
    }

    @Test
    public void testMaxWithRange2() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setRange("book1", "sheet1", "rango", "B2:B4");
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "A1", "= MAX(!sheet1.rango)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 7);
    }

    @Test
    public void testMaxWithRange3() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setRange("book1", "sheet1", "rango", "B4:C1");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setFormulaOf("book1", "sheet1", "C4", "9");
        app.setFormulaOf("book1", "sheet1", "A1", "= MAX(!sheet1.rango)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 9);
    }

    @Test
    public void testMin() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "6");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setFormulaOf("book1", "sheet1", "C4", "9");
        app.setFormulaOf("book1", "sheet1", "A1", "= MIN(B4:C1)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 0);
    }

    @Test
    public void testMin2() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setFormulaOf("book1", "sheet1", "C4", "9");
        app.setFormulaOf("book1", "sheet1", "A1", "= MIN(B4:B2)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 5);
    }

    @Test
    public void testMinWithRange() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setRange("book1", "sheet1", "rango", "B2:B4");
        app.setFormulaOf("book1", "sheet1", "A1", "= MIN(!sheet1.rango)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 5);
    }

    @Test
    public void testMinWithRange2() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setRange("book1", "sheet1", "rango", "B2:B4");
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "A1", "= MIN(!sheet1.rango)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 5);
    }

    @Test
    public void testMinWithRange3() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setRange("book1", "sheet1", "rango", "B2:B4");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setFormulaOf("book1", "sheet1", "B2", "4");
        app.setFormulaOf("book1", "sheet1", "A1", "= MIN(!sheet1.rango)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 4);
    }

    @Test
    public void testMinAndMaxWithRange() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setRange("book1", "sheet1", "rango", "B2:B4");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setFormulaOf("book1", "sheet1", "B2", "4");
        app.setFormulaOf("book1", "sheet1", "A1", "= MIN(B2:B4) - MAX(B2:B4)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 4 - 7);
    }

    @Test
    public void testMinAndMaxWithRange2() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setRange("book1", "sheet1", "rango", "B2:B4");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setFormulaOf("book1", "sheet1", "B2", "4");
        app.setFormulaOf("book1", "sheet1", "A1", "= MIN(!sheet1.rango) - MAX(!sheet1.rango)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 4 - 7);
    }

    @Test
    public void testMaxAndMinWithRange() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setRange("book1", "sheet1", "rango", "B2:B4");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setFormulaOf("book1", "sheet1", "B2", "4");
        app.setFormulaOf("book1", "sheet1", "A1", "= MAX(B2:B4) - MIN(B2:B4)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 7 - 4);
    }

    @Test
    public void testMaxAndMinWithRange2() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setRange("book1", "sheet1", "rango", "B2:B4");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setFormulaOf("book1", "sheet1", "B2", "4");
        app.setFormulaOf("book1", "sheet1", "A1", "= MAX(!sheet1.rango) - MIN(!sheet1.rango)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 7 - 4);
    }

    @Test
    public void testMaxAndMinWithRange3() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setRange("book1", "sheet1", "rango", "B2:B4");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setFormulaOf("book1", "sheet1", "B2", "4");
        app.setFormulaOf("book1", "sheet1", "A1", "= MAX(B2:B4) - MIN(!sheet1.rango)");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 7 - 4);
    }
    
    @Test
    public void testPrint() throws Exception{
        app.setFormulaOf("book1", "sheet1", "B2", "Martin");
        app.setFormulaOf("book1", "sheet1", "B3", "Nico");
        app.setFormulaOf("book1", "sheet1", "B4", "Nadia");
        app.setFormulaOf("book1", "sheet1", "A1", "= PRINT($1 $2 y $3 aprobaremos?,B2,B3,B4)");
        assertEquals(app.getCellContentAsString("book1", "sheet1", "A1"), "Martin Nico y Nadia aprobaremos?");
    }
    
    @Test
    public void testPrintNotEnoughReferences() throws Exception{
        app.setFormulaOf("book1", "sheet1", "B2", "Martin");
        app.setFormulaOf("book1", "sheet1", "B3", "Nico");
        app.setFormulaOf("book1", "sheet1", "B4", "Nadia");
        app.setFormulaOf("book1", "sheet1", "A1", "= PRINT($1 $2 muchas referencias!,B2)");
        assertEquals(app.getCellContentAsString("book1", "sheet1", "A1"), "");
    }

    @Ignore("Test que no funciona porque el AVERAGE NO FUNCIONA")
    @Test
    public void testAverage() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "6");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setFormulaOf("book1", "sheet1", "C4", "9");
        app.setFormulaOf("book1", "sheet1", "A1", "= AVERAGE(B4:C1)");
        assertEquals(app.getValueOf("book1", "sheet1", "A1"), ((6 + 5 + 7 + 9) / 8), DELTA);
    }

    @Ignore("Test que no funciona porque el AVERAGE NO FUNCIONA")
    @Test
    public void testAverage2() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "6");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setFormulaOf("book1", "sheet1", "C4", "9");
        app.setFormulaOf("book1", "sheet1", "A1", "= AVERAGE(B4:B2)");
        assertEquals(app.getValueOf("book1", "sheet1", "A1"), ((6 + 5 + 7 + 9) / 3), DELTA);
    }

    @Ignore("Test que no funciona porque el AVERAGE NO FUNCIONA")
    @Test
    public void testAverageWithRange() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "6");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setRange("book1", "sheet1", "rango", "B2:B4");
        app.setFormulaOf("book1", "sheet1", "A1", "= AVERAGE(!sheet1.rango)");
        assertEquals(app.getValueOf("book1", "sheet1", "A1"), (6 + 5 + 7) / 3, DELTA);
    }

    @Ignore("Test que no funciona porque el AVERAGE NO FUNCIONA")
    @Test
    public void testAverageWithRange2() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setRange("book1", "sheet1", "rango", "B2:B4");
        app.setFormulaOf("book1", "sheet1", "B2", "6");
        app.setFormulaOf("book1", "sheet1", "A1", "= AVERAGE(!sheet1.rango)");
        assertEquals(app.getValueOf("book1", "sheet1", "A1"), (5 + 7 + 6) / 3, DELTA);
    }

    @Ignore("Test que no funciona porque el AVERAGE NO FUNCIONA")
    @Test
    public void testAverageWithRange3() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "6");
        app.setFormulaOf("book1", "sheet1", "B3", "5");
        app.setRange("book1", "sheet1", "rango", "B2:B4");
        app.setFormulaOf("book1", "sheet1", "B4", "7");
        app.setFormulaOf("book1", "sheet1", "B2", "3");
        app.setFormulaOf("book1", "sheet1", "A1", "= AVERAGE(!sheet1.rango)");
        assertEquals(app.getValueOf("book1", "sheet1", "A1"), (3 + 7 + 5) / 3, DELTA);
    }

}
