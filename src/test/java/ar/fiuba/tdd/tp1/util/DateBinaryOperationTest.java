package ar.fiuba.tdd.tp1.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DateBinaryOperationTest {
    private static final Long ONE_DAY_MILLIS = (long) (1000 * 60 * 60 * 24);
    private static final DoubleBinaryOperation ADDITION = DoubleBinaryOperation::addition;
    private static final DoubleBinaryOperation SUBTRACT = DoubleBinaryOperation::subtraction;

    DateBinaryOperation dateBinaryAddition = DateBinaryOperation.create().withOperation(ADDITION);
    DateBinaryOperation dateBinarySubtraction = DateBinaryOperation.create()
            .withOperation(SUBTRACT);

    @Test
    public void testOperateDateAndNumber() {
        testOperateDateAndNumber1();

        testOperateDateAndNumber2();

        testOperateDateAndNumber3();

    }

	private void testOperateDateAndNumber3() {
		{
            GregorianCalendar gtoday = new GregorianCalendar();
            Date dtoday = gtoday.getTime();

            GregorianCalendar gyesterday = new GregorianCalendar();
            gyesterday.setTimeInMillis(dtoday.getTime() - ONE_DAY_MILLIS);

            Date resultDate = (Date) dateBinarySubtraction.operate(dtoday, ONE_DAY_MILLIS);

            GregorianCalendar gresult = new GregorianCalendar();
            gresult.setTime(resultDate);

            assertEquals(gyesterday.get(Calendar.DAY_OF_MONTH), gresult.get(Calendar.DAY_OF_MONTH));
        }
	}

	private void testOperateDateAndNumber2() {
		{
            GregorianCalendar gtoday = new GregorianCalendar();
            Date dtoday = gtoday.getTime();

            GregorianCalendar gtomorrow = new GregorianCalendar();
            gtomorrow.setTimeInMillis(dtoday.getTime() + ONE_DAY_MILLIS);

            Date resultDate = (Date) dateBinaryAddition.operate(ONE_DAY_MILLIS, dtoday);

            GregorianCalendar gresult = new GregorianCalendar();
            gresult.setTime(resultDate);

            assertEquals(gtomorrow.get(Calendar.DAY_OF_MONTH), gresult.get(Calendar.DAY_OF_MONTH));
        }
	}

	private void testOperateDateAndNumber1() {
		{
            GregorianCalendar gtoday = new GregorianCalendar();
            Date dtoday = gtoday.getTime();

            GregorianCalendar gtomorrow = new GregorianCalendar();
            gtomorrow.setTimeInMillis(dtoday.getTime() + ONE_DAY_MILLIS);

            Date resultDate = (Date) dateBinaryAddition.operate(dtoday, ONE_DAY_MILLIS);

            GregorianCalendar gresult = new GregorianCalendar();
            gresult.setTime(resultDate);

            assertEquals(gtomorrow.get(Calendar.DAY_OF_MONTH), gresult.get(Calendar.DAY_OF_MONTH));
        }
	}

    @Test
    public void testAddTwoDates() {
        GregorianCalendar gdate1 = new GregorianCalendar();
        GregorianCalendar gdate2 = new GregorianCalendar();

        long addMillis = gdate1.getTimeInMillis() + gdate2.getTimeInMillis();
        GregorianCalendar gexpected = new GregorianCalendar();
        gexpected.setTimeInMillis(addMillis);

        Date date1 = gdate1.getTime();
        Date date2 = gdate2.getTime();
        Date dactual = dateBinaryAddition.operate(date1, date2);

        assertTrue(DateComparator.compare().yearMonthDay(gexpected, dactual));
    }

}
