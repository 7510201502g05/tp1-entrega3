package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.BadFormulaException;
import ar.fiuba.tdd.tp1.acceptance.driver.BadReferenceException;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.UndeclaredWorkSheetException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FormulasTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadSheet();
        testDriver.createNewWorkBookNamed("book1");
        testDriver.createNewWorkSheetNamed("book1", "sheet1");
    }

    @Test
    public void sumLiterals() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + 0 + 3.5 + -1");

        assertEquals(3.5, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void sumLiteralsInDifferentRows() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "1");
        testDriver.setCellValue("tecnicas", "default", "B2", "2");
        testDriver.setCellValue("tecnicas", "default", "C3", "= A1 + B2");

        assertEquals(1 + 2, testDriver.getCellValueAsDouble("tecnicas", "default", "C3"), DELTA);
    }

    @Test
    public void subtractLiterals() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 - 0 - 3.5 - -1");

        assertEquals(1 - 3.5 - -1, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"),
                DELTA);
    }

    @Test
    public void formulaWithReferences() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2 + 0.5 - A3");
        testDriver.setCellValue("tecnicas", "default", "A2", "5");
        testDriver.setCellValue("tecnicas", "default", "A3", "2");

        assertEquals(1 + 5 + 0.5 - 2, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"),
                DELTA);
    }

    @Test
    public void formulaWithReferenceToReference() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "= 2 + A3");
        testDriver.setCellValue("tecnicas", "default", "A3", "3");

        assertEquals(1 + 2 + 3, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void formulaWithReferencesFromOtherSpreadSheet() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "other");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + !other.A2 + 0.5 - A3");
        testDriver.setCellValue("tecnicas", "other", "A2", "-1");
        testDriver.setCellValue("tecnicas", "default", "A3", "2");

        assertEquals(1 - 1 + 0.5 - 2, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"),
                DELTA);
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaStringAndNumber() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + hello");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaStringAndNumberRetrieveAsString() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + hello");
        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaWithReference() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "Hello");
        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaWithoutEqualSymbol() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "1 + 2");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = UndeclaredWorkSheetException.class)
    public void undeclaredWorkSheetInvocation() {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.getCellValueAsString("tecnicas", "undeclaredWorkSheet", "A1");
    }

    @Test(expected = BadReferenceException.class)
    public void formulaWithCyclicReferences() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "= A3");
        testDriver.setCellValue("tecnicas", "default", "A3", "= A1");
        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test
    public void formulaWithChangesInPreviousCells() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "= 8 + A1");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 10");
        assertEquals(8 + 10, testDriver.getCellValueAsDouble("tecnicas", "default", "A2"), DELTA);
    }

    @Test
    public void formulaWithAverageRangeCalculations() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= AVERAGE(A1:A4)");
        assertEquals((10 + 80 + 10 + 20) / 4,
                testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    /*
     * Agregado por el grupo 5 para demostrar que fallaba el tests anterior con valores diferentes
     * en una variable
     */
    @Ignore("Tests que falla porque esta mal el AVERAGE")
    @Test
    public void formulaWithAverageRangeCalculations2() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "1 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= AVERAGE(A1:A4)");
        assertEquals((1 + 80 + 10 + 20) / 4,
                testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    @Test
    public void formulaWithMaxRangeCalculations() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= MAX(A1:A4)");
        assertEquals(80, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    @Test
    public void formulaWithMinRangeCalculations() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= MIN(A1:A4)");
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    @Test
    public void formulaWithConcatCells() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "La edad es de ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", " años");
        testDriver.setCellValue("tecnicas", "default", "A5", "= CONCAT(A1,A2,A3)");
        assertEquals("La edad es de 80 años",
                testDriver.getCellValueAsString("tecnicas", "default", "A5"));
    }

    @Test
    public void formulaWithConcatCellsWithChangesInPreviousCells() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "La edad es de ");
        testDriver.setCellValue("tecnicas", "default", "B2", "80");
        testDriver.setCellValue("tecnicas", "default", "A2", "=B2");
        testDriver.setCellValue("tecnicas", "default", "A3", " años");
        testDriver.setCellValue("tecnicas", "default", "A5", "= CONCAT(A1,A2,A3)");
        testDriver.setCellValue("tecnicas", "default", "B2", "81");
        assertEquals("La edad es de 81 años",
                testDriver.getCellValueAsString("tecnicas", "default", "A5"));
    }

    //    mas tests

    @Test
    public void formulaWithMinRangeCalculations2() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= MIN(A1:A4)");
        testDriver.setCellValue("tecnicas", "default", "A3", "1");
        assertEquals(1, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    @Test
    public void sumLiterals3() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + 3.5 ");

        assertEquals(4.5, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void sumLiterals2() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + 0 + 3.5 - 1");

        assertEquals(3.5, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Ignore("Aun no testeado")
    @Test
    public void formulaWithMinNameRangeCalculations() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        //Declaro el rango A1:A4
        testDriver.setCellValue("tecnicas", "default", "A5", "= MIN(rango)");
        testDriver.setCellValue("tecnicas", "default", "A3", "1");
        assertEquals(1, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    //Mas tests del grupo 5

    @Test
    public void setRangeInSpreadSheet() throws Exception {
        testDriver.setRange("book1", "sheet1", "rango", "A1:A5");
        assertEquals(testDriver.getRange("book1", "sheet1", "rango"), "A1:A5");
    }

    @Test
    public void setRangeInSpreadSheetAndUndo() throws Exception {
        testDriver.setRange("book1", "sheet1", "rango", "A1:A5");
        testDriver.undo();
        assertEquals(testDriver.getRange("book1", "sheet1", "rango"), "");
    }

    @Test
    public void setAndRemoveRangeInSpreadSheet() throws Exception {
        testDriver.setRange("book1", "sheet1", "rango", "A1:A5");
        testDriver.removeRange("book1", "sheet1", "rango");
        assertEquals(testDriver.getRange("book1", "sheet1", "rango"), "");
    }

    @Test
    public void setAndRemoveRangeInSpreadSheetAndUndoAndRedo() throws Exception {
        testDriver.setRange("book1", "sheet1", "rango", "A1:A5");
        testDriver.removeRange("book1", "sheet1", "rango");
        testDriver.undo();
        assertEquals(testDriver.getRange("book1", "sheet1", "rango"), "A1:A5");
        testDriver.redo();
        assertEquals(testDriver.getRange("book1", "sheet1", "rango"), "");
    }

    @Test
    public void testMax2() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B2", "5");
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setCellValue("book1", "sheet1", "C4", "9");
        testDriver.setCellValue("book1", "sheet1", "A1", "= MAX(B4:C1)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 9);
    }

    @Test
    public void testMax3() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B2", "5");
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setRange("book1", "sheet1", "rango", "B2:B4");
        testDriver.setCellValue("book1", "sheet1", "A1", "= MAX(B2:B4)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 7);
    }

    @Test
    public void testMaxWithRange() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B2", "5");
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setRange("book1", "sheet1", "rango", "B2:B4");
        testDriver.setCellValue("book1", "sheet1", "A1", "= MAX(!sheet1.rango)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 7);
    }

    @Test
    public void testMaxWithRange2() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setRange("book1", "sheet1", "rango", "B2:B4");
        testDriver.setCellValue("book1", "sheet1", "B2", "5");
        testDriver.setCellValue("book1", "sheet1", "A1", "= MAX(!sheet1.rango)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 7);
    }

    @Test
    public void testMaxWithRange3() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B2", "5");
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setRange("book1", "sheet1", "rango", "B4:C1");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setCellValue("book1", "sheet1", "C4", "9");
        testDriver.setCellValue("book1", "sheet1", "A1", "= MAX(!sheet1.rango)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 9);
    }

    @Test
    public void testMin() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B2", "6");
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setCellValue("book1", "sheet1", "C4", "9");
        testDriver.setCellValue("book1", "sheet1", "A1", "= MIN(B4:C1)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 0);
    }

    @Test
    public void testMin2() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B2", "5");
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setCellValue("book1", "sheet1", "C4", "9");
        testDriver.setCellValue("book1", "sheet1", "A1", "= MIN(B4:B2)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 5);
    }

    @Test
    public void testMinWithRange() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B2", "5");
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setRange("book1", "sheet1", "rango", "B2:B4");
        testDriver.setCellValue("book1", "sheet1", "A1", "= MIN(!sheet1.rango)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 5);
    }

    @Test
    public void testMinWithRange2() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setRange("book1", "sheet1", "rango", "B2:B4");
        testDriver.setCellValue("book1", "sheet1", "B2", "5");
        testDriver.setCellValue("book1", "sheet1", "A1", "= MIN(!sheet1.rango)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 5);
    }

    @Test
    public void testMinWithRange3() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B2", "5");
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setRange("book1", "sheet1", "rango", "B2:B4");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setCellValue("book1", "sheet1", "B2", "4");
        testDriver.setCellValue("book1", "sheet1", "A1", "= MIN(!sheet1.rango)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 4);
    }

    @Test
    public void testMinAndMaxWithRange() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B2", "5");
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setRange("book1", "sheet1", "rango", "B2:B4");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setCellValue("book1", "sheet1", "B2", "4");
        testDriver.setCellValue("book1", "sheet1", "A1", "= MIN(B2:B4) - MAX(B2:B4)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 4 - 7);
    }

    @Test
    public void testMinAndMaxWithRange2() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B2", "5");
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setRange("book1", "sheet1", "rango", "B2:B4");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setCellValue("book1", "sheet1", "B2", "4");
        testDriver.setCellValue("book1", "sheet1", "A1",
                "= MIN(!sheet1.rango) - MAX(!sheet1.rango)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 4 - 7);
    }

    @Test
    public void testMaxAndMinWithRange() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B2", "5");
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setRange("book1", "sheet1", "rango", "B2:B4");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setCellValue("book1", "sheet1", "B2", "4");
        testDriver.setCellValue("book1", "sheet1", "A1", "= MAX(B2:B4) - MIN(B2:B4)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 7 - 4);
    }

    @Test
    public void testMaxAndMinWithRange2() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B2", "5");
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setRange("book1", "sheet1", "rango", "B2:B4");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setCellValue("book1", "sheet1", "B2", "4");
        testDriver.setCellValue("book1", "sheet1", "A1",
                "= MAX(!sheet1.rango) - MIN(!sheet1.rango)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 7 - 4);
    }

    @Test
    public void testMaxAndMinWithRange3() throws Exception {
        testDriver.setCellValue("book1", "sheet1", "B2", "5");
        testDriver.setCellValue("book1", "sheet1", "B3", "5");
        testDriver.setRange("book1", "sheet1", "rango", "B2:B4");
        testDriver.setCellValue("book1", "sheet1", "B4", "7");
        testDriver.setCellValue("book1", "sheet1", "B2", "4");
        testDriver.setCellValue("book1", "sheet1", "A1", "= MAX(B2:B4) - MIN(!sheet1.rango)");
        assertEquals((int) testDriver.getCellValueAsDouble("book1", "sheet1", "A1"), 7 - 4);
    }

    @Test
    public void testPrint() throws Exception {
        //        nuestro primer argumento es el $1
        testDriver.setCellValue("book1", "sheet1", "B2", "= PRINT(hola $1 $2,como,estas)");
        assertEquals("hola como estas", testDriver.getCellValueAsString("book1", "sheet1", "B2"));
    }

    @Test
    public void testPrint2() throws Exception {
        //        nuestro primer argumento es el $1
        testDriver.setCellValue("book1", "sheet1", "B2", "=PRINT(hola $1 como $2?,Nico,estas)");
        assertEquals("hola Nico como estas?",
                testDriver.getCellValueAsString("book1", "sheet1", "B2"));
    }
}