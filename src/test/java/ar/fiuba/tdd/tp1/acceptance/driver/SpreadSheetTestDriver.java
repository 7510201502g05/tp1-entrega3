package ar.fiuba.tdd.tp1.acceptance.driver;

import ar.fiuba.tdd.tp1.exceptions.NoBookException;
import ar.fiuba.tdd.tp1.exceptions.NoSheetsException;

import java.util.List;

public interface SpreadSheetTestDriver {

    List<String> workBooksNames();

    void createNewWorkBookNamed(String name);

    void createNewWorkSheetNamed(String workbookName, String name);

    List<String> workSheetNamesFor(String workBookName);

    void setCellValue(String workBookName, String workSheetName, String cellId, String value);

    String getCellValueAsString(String workBookName, String workSheetName, String cellId) throws BadFormatException;

    double getCellValueAsDouble(String workBookName, String workSheetName, String cellId) throws BadFormulaException, BadFormatException;

    void undo();

    void redo();

    void setCellType(String workBookName, String workSheetName, String cellId, String cellType);

    void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatterType, String formatterOption);

    void persistWorkBook(String bookName, String fileName);

    void reloadPersistedWorkBook(String fileName);

    void saveAsCSV(String workBookName, String workSheetName, String fileName);

    void loadFromCSV(String workBookName, String workSheetName, String fileName);
    
    /*Nuevo GRUPO 05*/
    
    /**
     * Guarda un rango con nombre en una hoja de un libro.
     * 
     * @param bookName : nombre del libro.
     * @param sheetName : nombre de la hoja.
     * @param name : nombre del rango.
     * @param range : rango.
     * @throws NoSheetsException : no existe la hoja que se quizo guardar el rango.
     * @throws NoBookException : no existe el libro que se quizo guardar el rango.
     */
    public void setRange(String bookName, String sheetName, String name, String range) throws NoSheetsException, NoBookException ;
    
    /**
     * Obtiene el rango de una hoja de un libro.
     * 
     * @param bookName : nombre del libro.
     * @param sheetName : nombre de la hoja.
     * @param name : nombre del rango.
     * @return el rango que tiene el mismo nombre que se paso por parametro en la hoja y el libro indicado.
     * @throws NoSheetsException : no existe la hoja en el libro.
     * @throws NoBookException : no existe el libro que se quizo acceder.
     */
    public String getRange(String bookName, String sheetName, String name) throws NoSheetsException, NoBookException;
    
    /**
     * Elimina un rango de una hoja de un libro.
     * 
     * @param bookName : nombre del libro.
     * @param sheetName : nombre de la hoja.
     * @param name : nombre del rango a eliminar.
     * @throws NoSheetsException : no existe la hoja del libro.
     * @throws NoBookException : no existe el libro.
     */
    public void removeRange(String bookName, String sheetName, String name) throws NoSheetsException, NoBookException;
    
    /***/
}