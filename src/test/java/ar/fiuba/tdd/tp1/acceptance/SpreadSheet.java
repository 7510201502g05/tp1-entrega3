package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.*;
import ar.fiuba.tdd.tp1.commands.NothingToRedoException;
import ar.fiuba.tdd.tp1.commands.NothingToUndoException;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellReferenceException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.NoBookException;
import ar.fiuba.tdd.tp1.exceptions.NoSheetsException;
import ar.fiuba.tdd.tp1.model.SpreadSheetApplication;
import ar.fiuba.tdd.tp1.persistence.CsvParser;

import java.util.List;

/**
 * Created by ale on 10/5/15.
 */
public class SpreadSheet implements SpreadSheetTestDriver {

    SpreadSheetApplication spreadSheet = new SpreadSheetApplication();

    @Override
    public List<String> workBooksNames() {
        return spreadSheet.getBooksNames();
    }

    @Override
    public void createNewWorkBookNamed(String name) {
        spreadSheet.addBook(name);
    }

    @Override
    public void createNewWorkSheetNamed(String workbookName, String name) {
        try {
            spreadSheet.addSheet(workbookName, name);
        } catch (Exception e) {
            return;
        }
    }

    @Override
    public List<String> workSheetNamesFor(String workBookName) {
        return spreadSheet.getSheetsNamesOfBook(workBookName);
    }

    @Override
    public void setCellValue(String workBookName, String workSheetName, String cellId, String value) {
        try {
            spreadSheet.setFormulaOf(workBookName, workSheetName, cellId, value);
        } catch (Exception e) {
            return;
        }
    }

    @Override
    public String getCellValueAsString(String workBookName, String workSheetName, String cellId) {
        try {
            return spreadSheet.getCellContentAsString(workBookName, workSheetName, cellId);
        } catch (NoSheetsException e) {
            throw new UndeclaredWorkSheetException();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public double getCellValueAsDouble(String workBookName, String workSheetName, String cellId) {
        try {
            return spreadSheet.getValueOf(workBookName, workSheetName, cellId);
        } catch (InvalidFormatException e) {
            throw new BadFormatException();
        } catch (InvalidCellReferenceException e2) {
            throw new BadReferenceException();
        } catch (Exception e) {
            throw new BadFormulaException();
        }
    }

    @Override
    public void undo() {
        try {
            spreadSheet.undo();
        } catch (NothingToUndoException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void redo() {
        try {
            spreadSheet.redo();
        } catch (NothingToRedoException e) {
            return;
        }
    }

    @Override
    public void setCellType(String workBookName, String workSheetName, String cellId, String cellType) {
        try {
            spreadSheet.setTypeOf(workBookName, workSheetName, cellId, cellType);
        } catch (Exception e) {
            return;
        }
    }

    @Override
    public void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatterType, String formatterOption) {
        try {
            spreadSheet.setFormatterOf(workBookName, workSheetName, cellId, formatterType, formatterOption);
        } catch (Exception e) {
            return;
        }
    }

    @Override
    public void persistWorkBook(String bookName, String fileName) {
        try {
            spreadSheet.persistBook(bookName, fileName);
        } catch (NoBookException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reloadPersistedWorkBook(String fileName) {
        spreadSheet.reloadBook(fileName);
    }

    @Override
    public void saveAsCSV(String workBookName, String workSheetName, String fileName) {
        CsvParser parser = new CsvParser();
        parser.dumpCSV(spreadSheet, workBookName, workSheetName, fileName);
    }

    @Override
    public void loadFromCSV(String workBookName, String workSheetName, String fileName) {
        CsvParser parser = new CsvParser();
        parser.loadCSV(spreadSheet, workBookName, workSheetName, fileName);
    }

    @Override
    public void setRange(String bookName, String sheetName, String name, String range)
            throws NoSheetsException, NoBookException {
        spreadSheet.setRange(bookName, sheetName, name, range);
    }

    @Override
    public String getRange(String bookName, String sheetName, String name)
            throws NoSheetsException, NoBookException {
        return spreadSheet.getRange(bookName, sheetName, name);
    }

    @Override
    public void removeRange(String bookName, String sheetName, String name)
            throws NoSheetsException, NoBookException {
        spreadSheet.removeRange(bookName, sheetName, name);
    }
}
