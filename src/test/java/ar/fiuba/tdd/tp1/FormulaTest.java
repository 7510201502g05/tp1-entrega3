package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellReferenceException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormulaException;
import ar.fiuba.tdd.tp1.model.formulas.Formula;
import ar.fiuba.tdd.tp1.model.formulas.ValueFormula;
import ar.fiuba.tdd.tp1.acceptance.SpreadSheet;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.model.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FormulaTest {

    private Formula formula;
    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadSheet();
        formula = new ValueFormula(651);
    }

    @Test
    public void testFormulaExists() {
        assertNotNull(formula);
    }

    @Test
    public void testFormulaEvaluable() throws InvalidFormulaException, InvalidFormatException, InvalidCellReferenceException {
        assertNotNull(formula.eval());
    }

    @Test
    public void formulaWithAverageRangeMinimal() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "= AVERAGE(A1:A1)");
        assertEquals((10) / 1, testDriver.getCellValueAsDouble("tecnicas", "default", "A2"), DELTA);
    }

    @Test
    public void formulaWithMaxRangeChanged() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A5", "= MAX(A1:A4)");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        assertEquals(80, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "A3", "100");
        assertEquals(100, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }


    @Test
    public void formulaWithMinRangeInversed() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= MIN(A4:A1)");
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    @Test
    public void formulaWithConcatCellsAndRange() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "La edad es de ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", " años");
        testDriver.setCellValue("tecnicas", "default", "A5", "= CONCAT(A1:A2,A3)");
        assertEquals("La edad es de 80 años", testDriver.getCellValueAsString("tecnicas", "default", "A5"));
    }

    @Test
    public void formulaWithAverageRangeSquareAndChanging() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "30 ");
        testDriver.setCellValue("tecnicas", "default", "B1", "22 ");
        testDriver.setCellValue("tecnicas", "default", "B2", "38 ");
        testDriver.setCellValue("tecnicas", "default", "A3", "= AVERAGE(A1:B2)");
        assertEquals((10 + 30 + 22 + 38) / 4, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "B1", "12 ");
        testDriver.setCellValue("tecnicas", "default", "B2", "28 ");
        assertEquals((10 + 30 + 12 + 28) / 4, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "A3", "= AVERAGE(A1:B1)");
        assertEquals((10 + 12) / 2, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"), DELTA);
    }

    @Test
    public void formulaWithCompositionAndRange() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "B1", "10");
        testDriver.setCellValue("tecnicas", "default", "B2", "20");
        testDriver.setCellValue("tecnicas", "default", "C1", "80");
        testDriver.setCellValue("tecnicas", "default", "C2", "20");
        testDriver.setCellValue("tecnicas", "default", "A3", "= MIN(A1:A2)");
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "B3", "= MIN(B1:B2)");
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "B3"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "C3", "= MIN(C1:C2)");
        assertEquals(20, testDriver.getCellValueAsDouble("tecnicas", "default", "C3"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "A4", "= MAX(A3:C3)");
        assertEquals(20, testDriver.getCellValueAsDouble("tecnicas", "default", "A4"), DELTA);
    }

    @Test
    public void formulaWithCompositionAndIndirectChanged() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "B1", "10");
        testDriver.setCellValue("tecnicas", "default", "B2", "20");
        testDriver.setCellValue("tecnicas", "default", "C1", "80");
        testDriver.setCellValue("tecnicas", "default", "C2", "20");
        testDriver.setCellValue("tecnicas", "default", "A3", "= MAX(A1:A2)");
        assertEquals(80, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "B3", "= MAX(B1:B2)");
        assertEquals(20, testDriver.getCellValueAsDouble("tecnicas", "default", "B3"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "C3", "= MAX(C1:C2)");
        assertEquals(80, testDriver.getCellValueAsDouble("tecnicas", "default", "C3"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "A4", "= AVERAGE(A3:C3)");
        assertEquals(( 80 + 20 + 80) / 3, testDriver.getCellValueAsDouble("tecnicas", "default", "A4"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "A2", "140");
        assertEquals(( 140 + 20 + 80) / 3, testDriver.getCellValueAsDouble("tecnicas", "default", "A4"), DELTA);
        testDriver.setCellValue("tecnicas", "default", "C2", "140");
        assertEquals(( 140 + 20 + 140) / 3, testDriver.getCellValueAsDouble("tecnicas", "default", "A4"), DELTA);
    }

}