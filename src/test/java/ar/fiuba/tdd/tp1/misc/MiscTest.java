package ar.fiuba.tdd.tp1.misc;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.*;

public class MiscTest {

    @Test
    public void testSumDates() throws ParseException {
        final long oneDay = 1000 * 60 * 60 * 24;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse("2015-10-28");
        Date tomorrow = new Date(date.getTime() + oneDay);
        GregorianCalendar gtomorrow = new GregorianCalendar();
        gtomorrow.setTime(tomorrow);
        assertEquals(gtomorrow.get(Calendar.DAY_OF_MONTH), 29);
    }

    @Test
    public void testSubstring() {
        String hol = "Hola".substring(0, 3);
        assertEquals("Hol", hol);
        System.out.println(hol);

        String inv = new StringBuilder(new StringBuilder("Hola").reverse().substring(0, 3))
                .reverse().toString();
        assertEquals("ola", inv);
    }
}
