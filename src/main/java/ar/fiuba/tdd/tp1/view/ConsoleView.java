package ar.fiuba.tdd.tp1.view;

import ar.fiuba.tdd.tp1.commands.NothingToRedoException;
import ar.fiuba.tdd.tp1.commands.NothingToUndoException;
import ar.fiuba.tdd.tp1.exceptions.NoBookException;
import ar.fiuba.tdd.tp1.exceptions.NoSheetsException;
import ar.fiuba.tdd.tp1.exceptions.NoValueException;
import ar.fiuba.tdd.tp1.model.SpreadSheetApplication;
import ar.fiuba.tdd.tp1.window.table.WindowTable;

import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by ale on 10/22/15.
 */
public class ConsoleView {
    private final SpreadSheetApplication app;
    // private final Console cmd;
    private HashMap<String, FunctionSpreadSheet> actions;
    private WindowTable frame = null;

    public ConsoleView(SpreadSheetApplication app) {
        this.app = app;
        actions = new HashMap<String, FunctionSpreadSheet>();
        addActions();
    }

    private void addActions() {
        addAddBookAction();
        addAddShhetAction();
        addPrintBooksAction();
        addPrintCellValueAction();
        addPrintSheetAction();
        addPrintSheetsOfBookAction();
        addRedoAction();
        addUndoAction();
        addSetCellValueAction();
        addAddNamedRangeAction();
        addRemoveNamedRangeAction();
        addHelpAction();
        addNewPrintSheetAction();
    }

    public void init() {
        String input = "";
        String[] separated;
        String action = "";
        // String args = "";
        while (!input.equals("exit")) {
            separated = input.split(" ");
            action = separated[0];
            // if (separated.length > 1) {
            // args = separated[1];
            // }
            if (actions.containsKey(action)) {
                // actions.get(action).apply(args);
                // NADIA GALLI: se envia ahora todos los parametros para que cada funcion use los
                // que necesita.
                actions.get(action).apply((Object[]) separated);
            }
            input = new Scanner(System.in, "utf-8").nextLine();
        }
    }

    private void addRemoveNamedRangeAction() {
        /* removeNamedRange workBook-sheetName-name */
        actions.put("removeNamedRange", new FunctionSpreadSheet() {
            @Override
            public Object apply(Object... obj) {
                String arguments = (String) obj[1];
                String[] args = arguments.split("-");
                // String workBook = args[0];
                // String workSheet = args[1];
                // String name = args[2];
                try {
                    app.removeRange(args[0], args[1], args[2]);
                } catch (NoBookException e) {
                    System.out.print("No existe ese libro!!\n");
                }
                return null;
            }
        });
    }

    private void addAddNamedRangeAction() {
        /* addNamedRange workBook-sheetName-name-range */
        actions.put("addNamedRange", new FunctionSpreadSheet() {
            @Override
            public Object apply(Object... obj) {
                String arguments = (String) obj[1];
                String[] args = arguments.split("-");
                // String workBook = args[0];
                // String workSheet = args[1];
                // String name = args[2];
                // String range = args[3];
                try {
                    app.setRange(args[0], args[1], args[2], args[3]);
                } catch (NoBookException e) {
                    System.out.print("No existe ese libro!!\n");
                }
                return null;
            }
        });
    }

    private void addHelpAction() {
        /* help */
        actions.put("help", new FunctionSpreadSheet() {
            @Override
            public Object apply(Object... obj) {
                System.out.println("Lista de comandos:");
                System.out.println("addBook workBook | Crea un libro");
                System.out.println("addSheet workBook-sheetName | Crea una hoja en un libro");
                System.out
                        .println("addNamedRange workBook-sheetName-nameRange-range | Crea un rango "
                                + "con nombre en una hoja de un libro");
                System.out
                        .println("removeNamedRange workBook-sheetName-nameRange | Elimina un rango "
                                + "con nombre en una hoja de un libro");
                System.out
                        .println("setCellValue workBook-workSheet-cellName-value | Inserta un valor"
                                + " en una celda de una hoja de un libro, el value puede tener espacios");
                System.out.println("undo | deshace la ultima accion");
                System.out.println("redo | reshace la ultima accion");
                System.out
                        .println("printSheet workBook-workSheet-cantFilas-cantColumnas | Muestra "
                                + "por pantalla la hoja solicitada con la cantidad de filas y columnas "
                                + "solicitadas");
                System.out
                        .println("newPrintSheet workBook-workSheet-cantFilas-cantColumnas | Muestra "
                                + "en una tabla la hoja solicitada con la cantidad de filas y columnas "
                                + "solicitadas");
                System.out.println("workBooks | Imprime los nombres de los libros");
                System.out
                        .println("workSheetsOf workBook | Imprime los nombres de las hojas de un libro");
                System.out.println("getCellValue workBook-workSheet-cellName-value | Imprime una "
                        + "celda solicitada de una hoja de un libro");
                return null;
            }
        });
    }

    private void addAddShhetAction() {
        /* addSheet workBook-sheetName */
        actions.put("addSheet", new FunctionSpreadSheet() {
            @Override
            public Object apply(Object... obj) {
                String arguments = (String) obj[1];
                String[] args = arguments.split("-");
                // String workBook = args[0];
                // String workSheet = args[1];
                try {
                    app.addSheet(args[0], args[1]);
                } catch (NoBookException e) {
                    System.out.print("No existe ese libro!!\n");
                }
                return null;
            }
        });
    }

    private void addAddBookAction() {
        /* addBook bookName */
        actions.put("addBook", new FunctionSpreadSheet() {
            @Override
            public Object apply(Object... obj) {
                String arguments = (String) obj[1];
                String[] args = arguments.split("-");
                // String bookName = args[0];
                app.addBook(args[0]);
                return null;
            }
        });
    }

    /**
     * Une con espacios el valor pasado por parametro y todos los argumentos pasados desde el objeto
     * 2.
     * 
     * @param value
     *            : valor inicial del valor.
     * @param args
     *            : son los parametro de una operacion.
     * @return un string que tiene el valor Value concatenado con espacios y los otros parametros.
     */
    private String getRawValue(String value, Object... args) {
        int length = args.length;
        if (length >= 3) {
            for (int i = 2; i < length; i++) {
                value = value.concat(" ").concat((String) args[i]);
            }
        }
        return value;
    }

    private void addSetCellValueAction() {
        /* setCellValue workBook-workSheet-cellName-value */
        actions.put("setCellValue", new FunctionSpreadSheet() {
            @Override
            public Object apply(Object... obj) {
                String arguments = (String) obj[1];
                String[] args = arguments.split("-");
                // String workBook = args[0];
                // String workSheet = args[1];
                // String cellName = args[2];
                // String value =args[3];
                // NADIA GALLI: Ahora entiene valores con espacios.
                String value = getRawValue(args[3], obj);
                try {
                    app.setFormulaOf(args[0], args[1], args[2], value);
                } catch (NoSheetsException e) {
                    System.out.print("No hay esa hoja!\n");
                } catch (NoBookException e) {
                    System.out.print("No hay ese libro!\n");
                }
                return null;
            }
        });
    }

    private void addUndoAction() {
        /* undo */
        actions.put("undo", new FunctionSpreadSheet() {
            @Override
            public Object apply(Object... obj) {
                try {
                    app.undo();
                } catch (NothingToUndoException e) {
                    System.out.print("No hay nada que deshacer!\n");
                }
                return null;
            }
        });
    }

    private void addRedoAction() {
        /* redo */
        actions.put("redo", new FunctionSpreadSheet() {
            @Override
            public Object apply(Object... obj) {
                try {
                    app.redo();
                } catch (NothingToRedoException e) {
                    System.out.print("No hay nada que rehacer!\n");
                }
                return null;
            }
        });
    }

    private void addPrintSheetAction() {
        /* printSheet workBook-workSheet-cantFilas-cantColumnas */
        actions.put("printSheet", new FunctionSpreadSheet() {
            @Override
            public Object apply(Object... obj) {
                String arguments = (String) obj[1];
                String[] args = arguments.split("-");
                // String workBook = args[0];
                // String workSheet = args[1];
                int rowsToPrint = Integer.parseInt(args[2]);
                int colsToPrint = Integer.parseInt(args[3]);

                printSheet(colsToPrint, rowsToPrint, args[0], args[1]);

                return null;
            }
        });
    }

    /**
     * NADIA GALLI: Agregue este metodo para imprimir la tabla que habiamos creado en el tp
     * anterior.
     */
    private void addNewPrintSheetAction() {
        /* newPrintSheet workBook-workSheet-cantFilas-cantColumnas */
        actions.put("newPrintSheet", new FunctionSpreadSheet() {
            @Override
            public Object apply(Object... obj) {
                String arguments = (String) obj[1];
                String[] args = arguments.split("-");
                // String workBook = args[0];
                // String workSheet = args[1];
                int rowsToPrint = Integer.parseInt(args[2]);
                int colsToPrint = Integer.parseInt(args[3]);
                TableContainerSpreadSheet tableContainer = new TableContainerSpreadSheet(
                        colsToPrint + 1, rowsToPrint + 1, args[0], args[1], app);
                if (frame == null) {
                    frame = new WindowTable(tableContainer);
                } else {
                    frame.changeModel(tableContainer);
                }
                frame.printWindow();
                return null;
            }
        });
    }

    private void printSheet(int colsToPrint, int rowsToPrint, String workBook, String workSheet) {
        /* Imprimo encabezado */
        System.out.print("   ");
        for (int col = 1; col <= colsToPrint; ++col) {
            System.out.print(getCharForNumber(col));
            System.out.print("   ");
        }
        System.out.println("");
        // GRUPO 05: enderezamos la tabla
        /* Imprimo tabla */
        for (int row = 1; row <= rowsToPrint; ++row) { // (int col = 1; col <= colsToPrint; ++col) {
            System.out.print(String.valueOf(row));// (col));
            System.out.print("  ");
            for (int col = 1; col <= colsToPrint; ++col) { // (int row = 1; row <= rowsToPrint;
                                                          // ++row) {
                String name = getCharForNumber(col) + String.valueOf(row);
                printCell(workBook, workSheet, name);
            }
            System.out.println("");
        }
    }

    private void printCell(String workBook, String workSheet, String name) {
        try {
            // GRUPO 05: hacemos que se imprima el string y no el double.
            // System.out.print(app.getValueOf(workBook, workSheet, name));
            System.out.print(app.getCellContentAsString(workBook, workSheet, name));
            System.out.print(" ");
        } catch (Exception e) {
            System.out.print(" ");
        }
    }

    private void addPrintBooksAction() {
        /* workBooks */
        actions.put("workBooks", new FunctionSpreadSheet() {
            @Override
            public Object apply(Object... obj) {
                for (String book : app.getBooksNames()) {
                    System.out.print(book + "\n");
                }
                return null;
            }
        });
    }

    private void addPrintSheetsOfBookAction() {
        /* workSheetsOf workBook */
        actions.put("workSheetsOf", new FunctionSpreadSheet() {
            @Override
            public Object apply(Object... obj) {
                String arguments = (String) obj[1];
                String[] args = arguments.split("-");
                // String workBook = args[0];
                for (String sheet : app.getSheetsNamesOfBook(args[0])) {
                    System.out.print(sheet + "\n");
                }
                return null;
            }
        });
    }

    private void addPrintCellValueAction() {
        /* getCellValue workBook-workSheet-cellName-value */
        actions.put("getCellValue", new FunctionSpreadSheet() {
            @Override
            public Object apply(Object... obj) {
                String arguments = (String) obj[1];
                String[] args = arguments.split("-");
                // String workBook = args[0];
                // String workSheet = args[1];
                // String cellName = args[2];
                try {
                    System.out.print(app.getCellContentAsString(args[0], args[1], args[2]));
                } catch (NoValueException e) {
                    System.out.print(" \n");
                } catch (Exception e) {
                    System.out.print(e.getMessage());
                }
                return null;
            }
        });
    }

    /**
     * Returns the Alfabet Leter Corresponding to the @number.
     *
     * @param number
     *            a number
     * @return a Letter
     */
    private String getCharForNumber(int number) {
        return number > 0 && number < 27 ? String.valueOf((char) (number + 'A' - 1)) : null;
    }
}
