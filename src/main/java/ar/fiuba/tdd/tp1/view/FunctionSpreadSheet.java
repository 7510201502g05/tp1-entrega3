package ar.fiuba.tdd.tp1.view;

/**
 * Interfaz que deben implementar los comandos por consola.
 * 
 * @author Galli
 *
 */
public interface FunctionSpreadSheet {

    /**
     * Resuelve la funcion con los parametros que le pasaron.
     * 
     * @param obj
     *            : parametros que ingreso el usuario.
     * @return un boject que es la solucion de aplicar la funcion al parametro.
     */
    public Object apply(Object... obj);

}
