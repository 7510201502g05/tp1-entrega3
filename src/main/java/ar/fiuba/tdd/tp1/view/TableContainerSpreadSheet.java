package ar.fiuba.tdd.tp1.view;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellReferenceException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormulaException;
import ar.fiuba.tdd.tp1.exceptions.NoBookException;
import ar.fiuba.tdd.tp1.exceptions.NoSheetsException;
import ar.fiuba.tdd.tp1.exceptions.NoValueException;
import ar.fiuba.tdd.tp1.model.SpreadSheetApplication;
import ar.fiuba.tdd.tp1.window.table.ColumnSequence;
import ar.fiuba.tdd.tp1.window.table.NestedColumnSequence;
import ar.fiuba.tdd.tp1.window.table.TableContainer;

import javax.swing.table.DefaultTableModel;

/**
 * Esta clase se encarga de setearle los valores de una hoja de un libro del SpreadSheet a una
 * DefaultTableModel para usarse en la impresion de la tabla.
 * 
 * @author Galli
 *
 */
public class TableContainerSpreadSheet implements TableContainer {

    private int numColumn;
    private int numberRow;
    private String bookNameModified;
    private String sheetNameModified;
    private SpreadSheetApplication app;

    public TableContainerSpreadSheet(int colsToPrint, int rowsToPrint, String workBook,
            String workSheet, SpreadSheetApplication app) {
        this.app = app;
        this.numColumn = colsToPrint;
        this.numberRow = rowsToPrint;
        this.bookNameModified = workBook;
        this.sheetNameModified = workSheet;
    }

    @Override
    public DefaultTableModel addDataToTableModel(DefaultTableModel dtm) {
        dtm = addColumnsToTableModel(dtm);
        return addCellDataToTableModel(dtm);
    }

    /**
     * Agrega los valores de la hoja del libro a la DefaultTableModel.
     * 
     * @param dtm
     *            : DefaultTableModel que va a usar la WindowTable para imprimir la tabla.
     * @return DefaultTableModel con los datos de la tabla.
     */
    private DefaultTableModel addCellDataToTableModel(DefaultTableModel dtm) {
        Object[] table = new Object[numColumn];
        for (int i = 1; i < numberRow; i++) {
            table[0] = i;
            for (int j = 1; j < numColumn; j++) {
                table[j] = getValue(j, i);
            }
            dtm.addRow(table);
        }
        return dtm;
    }

    /**
     * Devuelve el valor que guarda una celda de la hoja del libro que se quiere imprimir.
     * 
     * @param column
     *            : numero de la columna de la celda.
     * @param row
     *            : numero de la fila de la celda
     * @return el contenido de la celda en un String.
     */
    private String getValue(int column, int row) {
        ColumnSequence columnSequence = NestedColumnSequence.create("A");
        for (int i = 1; i < column; i++) {
            columnSequence.next();
        }
        String id = columnSequence.getCurrent() + row;
        try {
            return app.getCellContentAsString(bookNameModified, sheetNameModified, id);
        } catch (NoSheetsException | NoValueException | InvalidFormulaException | NoBookException
                | InvalidFormatException | InvalidCellReferenceException e) {
            return "ERROR";
        }
    }

    /**
     * Agrega al dtm las columnas que debe tener la tabla.
     * 
     * @param dtm
     *            : modelo de tabla.
     * @return dtm modificado.
     */
    private DefaultTableModel addColumnsToTableModel(DefaultTableModel dtm) {
        dtm.addColumn(bookNameModified + "." + sheetNameModified);
        ColumnSequence columnSequence = NestedColumnSequence.create("A");
        for (int i = 1; i < numColumn; i++) {
            dtm.addColumn(columnSequence.getCurrent());
            columnSequence.next();
        }
        return dtm;
    }

}
