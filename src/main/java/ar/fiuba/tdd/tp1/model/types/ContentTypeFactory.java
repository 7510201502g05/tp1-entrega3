package ar.fiuba.tdd.tp1.model.types;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Leandro on 23-Oct-15. :)
 */
public class ContentTypeFactory {
    private SimpleDateFormat dateDefaultFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
    private SimpleDateFormat dateRequiredFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private Map<String, ContentType> contentTypes;

    public ContentTypeFactory() {
        contentTypes = new HashMap<>();
        addDefaultsContentTypes();
    }

    public ContentType getContentType(final String typeName) {
        if (contentTypes.containsKey(typeName)) {
            return contentTypes.get(typeName);
        }
        return getDefaultContentType();
    }

    private ContentType getDefaultContentType() {
        return new ContentType() {
            @Override
            public String getContentAsString(String originalInput, String contentRepresentation, String formattedContent) {
                //si es fecha lo paso al requerido.
                try {
                    return dateRequiredFormat.format(dateDefaultFormat.parse(contentRepresentation));
                } catch (Exception e) {
                    return formattedContent;
                }
            }

            @Override
            public String getType() {
                return "default";
            }
        };
    }

    public void addContentType(final String typeName, final ContentType contentType) {
        contentTypes.put(typeName, contentType);
    }

    private void addDefaultsContentTypes() {
        addContentType("Date", new TypeDate());

        addContentType("Currency", new TypeCurrency());

        addContentType("String", new TypeString());
    }
}
