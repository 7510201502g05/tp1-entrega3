package ar.fiuba.tdd.tp1.model.types;

import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;

/**
 * Created by ale on 10/26/15.
 */
public class TypeString implements ContentType {
    String typeName = "String";

    @Override
    public String getContentAsString(String originalInput, String contentRepresentation, String formattedContent) {
        return originalInput;
    }

    @Override
    public double getContentAsDouble(Object content) throws InvalidFormatException {
        throw new InvalidFormatException();
    }

    @Override
    public String getType() {
        return typeName;
    }
}
