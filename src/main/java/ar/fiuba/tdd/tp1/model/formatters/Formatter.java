package ar.fiuba.tdd.tp1.model.formatters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Leandro on 19-Oct-15. :)
 */
public abstract class Formatter {

    private String modifiedField;
    private Formatter originalFormatter;

    public Formatter(String modifiedField, Formatter originalFormatter) {
        this.modifiedField = modifiedField;
        this.originalFormatter = originalFormatter;
    }

    public String format(Object content, String... modifiedFields) {
        List<String> modifiedFieldsList = new ArrayList<>();
        Collections.addAll(modifiedFieldsList, modifiedFields);
        //Si ya se modifico ese campo, unicamente llamo al formatter original.
        if (modifiedFieldsList.contains(modifiedField)) {
            return originalFormatter(content, modifiedFields);
        } else {
            modifiedFieldsList.add(modifiedField);
            try {
                return modifyFormat(content, originalFormatter(content, modifiedFieldsList.toArray(new String[modifiedFieldsList.size()])));
            } catch (Exception e) {
                return originalFormatter(content, modifiedFields);
            }
        }
    }

    abstract String modifyFormat(Object content, String originalFormat);

    String originalFormatter(Object content, String... modifiedFields) {
        return originalFormatter.format(content, modifiedFields);
    }
}
