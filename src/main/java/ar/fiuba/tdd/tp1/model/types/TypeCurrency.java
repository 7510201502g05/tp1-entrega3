package ar.fiuba.tdd.tp1.model.types;

/**
 * Created by ale on 10/26/15.
 */
public class TypeCurrency implements ContentType {
    String typeName = "Currency";

    @Override
    public String getContentAsString(String originalInput, String contentRepresentation, String formattedContent) {
        try {
            Double.parseDouble(contentRepresentation);
        } catch (Exception e) {
            return "Error:BAD_CURRENCY";
        }
        return formattedContent;
    }

    @Override
    public String getType() {
        return typeName;
    }
}
