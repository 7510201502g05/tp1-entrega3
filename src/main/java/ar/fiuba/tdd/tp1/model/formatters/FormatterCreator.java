package ar.fiuba.tdd.tp1.model.formatters;

import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;

/**
 * Created by Leandro on 19-Oct-15. :)
 */
public interface FormatterCreator {
    public Formatter makeFormatter(Formatter oldFormatter, final String... options) throws InvalidFormatException;
}
