package ar.fiuba.tdd.tp1.model.types;

import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;

/**
 * Created by Leandro on 23-Oct-15. :)
 */
public interface ContentType {

    String getContentAsString(final String originalInput, final String contentRepresentation, final String formattedContent);

    default double getContentAsDouble(final Object content) throws InvalidFormatException {
        return ((Number) content).doubleValue();
    }

    String getType();
}
