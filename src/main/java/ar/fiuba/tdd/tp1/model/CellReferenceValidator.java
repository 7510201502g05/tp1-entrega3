package ar.fiuba.tdd.tp1.model;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellReferenceException;

/**
 * Created by Leandro on 24-Oct-15. :)
 */
public interface CellReferenceValidator {

    void addValidReference(final String startsSheetName, final String startsCellName, final String endSheetName,
                           final String endCellName) throws InvalidCellReferenceException;
}
