package ar.fiuba.tdd.tp1.model.formulas;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellReferenceException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormulaException;
import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.formatters.Formatter;
import ar.fiuba.tdd.tp1.model.formatters.FormatterFactory;
import ar.fiuba.tdd.tp1.model.types.ContentType;
import ar.fiuba.tdd.tp1.model.types.ContentTypeFactory;
import ar.fiuba.tdd.tp1.model.types.TypeDate;
import ar.fiuba.tdd.tp1.parsers.CollaborativeParserOrderer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Leandro on 17-Oct-15. :)
 */
public class CellContent implements Formula {

    List<String[]> formatsOfContent = new ArrayList<>();
    private transient Formula containedFormula = new ValueFormula(0);
    private String formulaRepresentation = "";
    private ContentType contentType = new ContentTypeFactory().getContentType("default");
    private transient Formatter formatter = new FormatterFactory().getTypeInfependentFormatter();

    public void setFormatter(String... formatOptions) {
        try {
            this.formatter = new FormatterFactory().getFormatter(formatter, formatOptions);
            formatsOfContent.add(formatOptions);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    /**
     * Retorna el valor evaluado como Object (sin transformar a string o a numero).
     * 
     * @return Valor valor evaluado como Object.
     * @throws InvalidCellReferenceException 
     * @throws InvalidFormatException 
     * @throws InvalidFormulaException 
     */
    public Object getValue() throws InvalidFormulaException, InvalidFormatException, InvalidCellReferenceException {
        return containedFormula.eval();
    }

    /**
     * Retorna el valor evaluado como String y con formato aplicado.
     * 
     * @return valor evaluado como String y con formato aplicado.
     * @throws InvalidFormatException
     * @throws InvalidCellReferenceException
     */
    public String getValueAsString() throws InvalidFormatException, InvalidCellReferenceException {
        try {
            /*
             * AGREGO ESTA PORCION DE CODIGO PARA CORREGIR EL ERROR DE FORMATOS DE
             * FECHAS (FORMATEO DE FECHAS FALLA EN MAQUINAS EN CASTELLANO). EL SIGUIENTE CONDICIONAL
             * VERIFICA QUE SI EL TIPO DE CELDA ES FECHA PERO EL CONTENIDO EVALUADO NO ES DE TIPO
             * Date ENTONCES LA FECHA INGRESADA ES INVALIDA Y SE DEBE RETORNAR BAD_DATE...
             */
            Object evalContent = containedFormula.eval();
            if (contentType.getClass().equals(TypeDate.class)) {
                if (!evalContent.getClass().equals(Date.class)) {
                    return "Error:BAD_DATE";
                }
                return formatter.format(evalContent);
            }

            /*
             * SI EL TIPO DE DATO EVALUADO ES Date PERO NO HAY UN FORMATO ASIGNADO
             * -> SE DEVUELVE LA REPRESENTACION ORIGINAL.
             */
            if (evalContent.getClass().equals(Date.class)) {
                return formulaRepresentation;
            }

            String evaluatedContentAsString = evalContent.toString();
            String formattedContent = formatter.format(evalContent);

            return contentType.getContentAsString(formulaRepresentation, evaluatedContentAsString,
                    formattedContent);
        } catch (InvalidFormulaException e) {
            return "";
        }
    }

    public double getValueAsDouble() throws InvalidFormulaException, InvalidFormatException,
            InvalidCellReferenceException {
        return contentType.getContentAsDouble(eval());
    }

    public void setContent(Book book, final String representation) {
        formulaRepresentation = representation;
        containedFormula = new CollaborativeParserOrderer().getCollaborativeParser(book).parse(
                representation);
    }

    public void setFormulaRepresentation(String representation) {
        formulaRepresentation = representation;
    }

    public void updateContent(Book book) {
        if (!formulaRepresentation.equals("")) {
            this.setContent(book, formulaRepresentation);
        }
    }

    @Override
    public Object eval() throws InvalidFormulaException, InvalidFormatException,
            InvalidCellReferenceException {
        return containedFormula.eval();
    }

    public final String getRepresentation() {
        return formulaRepresentation;
    }

    public void setContentType(String contentType) {
        this.contentType = new ContentTypeFactory().getContentType(contentType);
    }

    @Override
    public String toString() {
        return "CellContent{formulaRepresentation=" + formulaRepresentation + "}";
    }
}