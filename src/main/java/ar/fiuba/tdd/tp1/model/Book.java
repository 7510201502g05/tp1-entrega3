package ar.fiuba.tdd.tp1.model;

import ar.fiuba.tdd.tp1.exceptions.*;
import ar.fiuba.tdd.tp1.model.formulas.Formula;
import ar.fiuba.tdd.tp1.model.formulas.InvalidFormula;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * La clase Book se encarga de mantener las Sheets de la aplicacion.
 */
public class Book {

    private transient HashMappedSheet workingSheet;
    private transient Cell workingCell;
    private transient CellReferenceValidator cellReferenceValidator;
    private String bookName = "default";
    private List<HashMappedSheet> sheets;

    /**
     * Book constructor. It will have One Sheet with name "default"
     *
     * @param bookName
     *            the name of the new Book
     */
    public Book(String bookName) {
        this.bookName = bookName;
        sheets = new ArrayList<>();
        workingSheet = new HashMappedSheet();
        sheets.add(workingSheet);
        cellReferenceValidator = new GraphCellReferenceValidator();
    }

    /**
     * Book constructor. The Book's name will be "default" It will have One Sheet with name
     * "default"
     */
    public Book() {
        sheets = new ArrayList<>();
        workingSheet = new HashMappedSheet();
        sheets.add(workingSheet);
        cellReferenceValidator = new GraphCellReferenceValidator();
    }

    public void setSheets(List<HashMappedSheet> sheets) {
        this.sheets = sheets;
        workingSheet = sheets.get(0);
    }

    public void setWorkingCell(Cell workingCell) {
        this.workingCell = workingCell;
    }

    /**
     * Returns the Book's name.
     *
     * @return the Book's name
     */
    public String getName() {
        return bookName;
    }

    /**
     * Adds a Sheet to the Book.
     *
     * @param sheetName
     *            the new Sheet's name
     */
    public void addSheet(String sheetName) {
        sheets.add(new HashMappedSheet(sheetName, sheets.size() + 1));
    }

    /**
     * Adds a Sheet to the Book in the position @pos of the Collection.
     *
     * @param pos
     *            position of the Sheet
     * @param sheet
     *            Sheet instance to add
     */
    public void addSheet(int pos, HashMappedSheet sheet) {
        if (sheets.size() < pos) {
            sheets.add(sheet);
        } else if (pos < 0) {
            sheets.add(0, sheet);
        } else {
            sheets.add(pos - 1, sheet);
        }
    }

    /**
     * Returns a Sheet of the book.
     *
     * @param sheetName
     *            the Sheet's name
     * @return the Sheet instance
     * @throws NoSheetsException
     *             if there is no Sheet with such @sheetName
     */
    private HashMappedSheet getSheet(String sheetName) throws NoSheetsException {
        for (HashMappedSheet sheet : sheets) {
            if (sheet.getName().equals(sheetName)) {
                return sheet;
            }
        }
        throw new NoSheetsException();
    }

    public ArrayList<String> getSheetsNames() {
        ArrayList<String> names = new ArrayList<>();
        for (HashMappedSheet sheet : sheets) {
            names.add(sheet.getName());
        }
        return names;
    }

    /**
     * Removes the Sheet.
     *
     * @param sheetName
     *            the Sheet's name
     * @return the Sheet instance if it's in the Book
     * @throws NoSheetsException
     *             if the Sheet isn't in the Book
     */
    public HashMappedSheet removeSheet(String sheetName) throws NoSheetsException {
        HashMappedSheet sheet = getSheet(sheetName);
        sheets.remove(sheet);
        return sheet;
    }

    /**
     * Removes the las Sheet added.
     *
     * @return the Sheet instance
     * @throws NoSheetsException
     *             if there aren't anyy Sheets
     */
    public HashMappedSheet removeLastSheet() throws NoSheetsException {
        if (sheets.size() > 0) {
            return sheets.remove(sheets.size() - 1);
        }
        throw new NoSheetsException();
    }

    /**
     * Gets the numerical value of the Cell.
     *
     * @param sheetName
     *            the Sheet's name
     * @param cellName
     *            the Cell's name
     * @return the calculated value of the Cell
     * @throws NoValueException
     *             if there is NO formula in the Cell
     * @throws NoSheetsException
     *             If there is NO such Sheet
     * @throws InvalidFormulaException
     *             If the Formula is not valid
     * @throws InvalidFormatException
     *             If the Format cannot be evaluated.
     */
    public double getValueOf(String sheetName, String cellName) throws NoValueException,
            NoSheetsException, InvalidFormulaException, InvalidFormatException,
            InvalidCellReferenceException, NoCellException {
        return getSheet(sheetName).getValueOf(cellName);
    }

    /**
     * Returns the Formula instance contained in that Cell.
     *
     * @param sheetName
     *            the Sheet's name
     * @param cellName
     *            the Cell's name
     * @return the Formula instance
     * @throws NoSheetsException
     *             if there is no such Sheet
     */
    public Formula getReferenceOf(String sheetName, String cellName) throws NoSheetsException,
            NoCellException {
        // TODO: validar nombres de celdas!
        try {
            cellReferenceValidator.addValidReference(workingSheet.getName(), workingCell.getName(),
                    sheetName, cellName);
        } catch (InvalidCellReferenceException e) {
            return new InvalidFormula() {
                @Override
                public Object eval() throws InvalidFormulaException, InvalidCellReferenceException {
                    throw new InvalidCellReferenceException();
                }
            };
        }

        return getSheet(sheetName).getReferenceOf(cellName);
    }

    public List<Formula> getRangeReferenceOf(String[] cellIni, String[] cellFin)
            throws NoSheetsException {
        String sheetName = workingSheet.getName();
        try {
            cellReferenceValidator.addValidReference(sheetName, workingCell.getName(), sheetName,
                    Arrays.toString(cellIni));
            cellReferenceValidator.addValidReference(sheetName, workingCell.getName(), sheetName,
                    Arrays.toString(cellFin));
        } catch (InvalidCellReferenceException e) {
            List<Formula> subFormulas = new ArrayList<>();
            subFormulas.add(new InvalidFormula() {
                @Override
                public Object eval() throws InvalidFormulaException, InvalidCellReferenceException {
                    throw new InvalidCellReferenceException();
                }

            });
            return subFormulas;

        }

        HashMappedSheet sheet = getSheet(sheetName);
        return sheet.getRangeReferencesOf(cellIni, cellFin);
    }

    /**
     * returns the Cell instance.
     *
     * @param sheetName
     *            the Sheet's name
     * @param cellName
     *            the Cell's name
     * @return the Cell instance
     * @throws NoSheetsException
     *             if there is no such Sheet
     */
    public Cell getCell(String sheetName, String cellName) throws NoSheetsException {
        return getSheet(sheetName).getCell(cellName);
    }

    public String getWorkingSheet() {
        return this.workingSheet.getName();
    }

    public void setWorkingSheet(String workingSheet) throws NoSheetsException {
        this.workingSheet = getSheet(workingSheet);
    }

    public void updateCellsContent() {
        for (HashMappedSheet sheet : sheets) {
            workingSheet = sheet;
            sheet.updateCellsContent(this);
        }
    }

    public int getMaxRowsOfSheet(String sheet) throws NoSheetsException {
        return getSheet(sheet).getMaxRow();
    }

    public List<String> getRowValuesOf(String sheet, int row) throws NoSheetsException {
        return getSheet(sheet).getRowAsString(row);
    }

    /* Nuevos metodos del grupo 05 */

    /**
     * Guarda un nombre de rango en la hoja actual.
     * 
     * @param rangeName
     *            : nombre del rango.
     * @param range
     *            : rango.
     */
    public void setRange(String rangeName, String range) {
        workingSheet.setRange(rangeName, range);
    }

    /**
     * Obtiene el rango a partir de su nombre y el nombre de la hoja.
     * 
     * @param rangeName
     *            : nombre del rango.
     */
    public String getRange(String sheetName, String rangeName) {
        return getSheet(sheetName).getRange(rangeName);
    }

    /**
     * Elimina un rango de la hoja actual.
     * 
     * @param rangeName
     *            : nombre del rango a eliminar.
     */
    public void removeRange(String rangeName) {
        workingSheet.removeRange(rangeName);
    }

    //
}
