package ar.fiuba.tdd.tp1.model.formulas;

/**
 * Created by Leandro on 03-Oct-15. :)
 */
public class ValueFormula implements Formula {

    private Object formulaValue;

    /**
     * ValueFormula constructor.
     *
     * @param value the value.
     */
    public ValueFormula(final Object value) {
        this.formulaValue = value;
    }

    @Override
    public final Object eval() {
        return formulaValue;
    }

    @Override
    public String toString() {
        return "ValueFormula{formulaValue=" + formulaValue + "}";
    }
}
