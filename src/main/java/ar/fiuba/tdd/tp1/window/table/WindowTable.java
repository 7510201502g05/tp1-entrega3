package ar.fiuba.tdd.tp1.window.table;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Esta clase crea una ventana con la tabla y permite visualizarla.
 * 
 * @author Galli
 *
 */
public class WindowTable extends JFrame {

    private JTable table;

    public WindowTable(TableContainer tableContainer) {
        DefaultTableModel tableModel = new DefaultTableModel();
        table = new JTable(tableModel);
        setModel(tableModel, tableContainer);
        table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        JScrollPane scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        getContentPane().add(scrollPane, BorderLayout.CENTER);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });
    }

    /**
     * Setea un modelo a la tablaModel a partir de los datos del tableContainer.
     * 
     * @param tableModel
     *            : tabla para presentar por pantalla.
     * @param tableContainer
     *            : tabla con los datos que se quiere presentar por pantalla.
     */
    private void setModel(DefaultTableModel tableModel, TableContainer tableContainer) {
        tableContainer.addDataToTableModel(tableModel);
        table.setModel(tableModel);
    }

    /**
     * Modifica la tabla con los datos del tableContainer.
     * 
     * @param tableContainer
     *            : tabla que contiene los datos.
     */
    public void changeModel(TableContainer tableContainer) {
        this.setModel(new DefaultTableModel(), tableContainer);
    }

    /**
     * Abre la ventana con la tabla.
     */
    public void printWindow() {
        pack();
        setVisible(true);
    }

}
