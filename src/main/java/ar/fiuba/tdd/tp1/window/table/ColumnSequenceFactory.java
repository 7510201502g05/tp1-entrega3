package ar.fiuba.tdd.tp1.window.table;

@FunctionalInterface
public interface ColumnSequenceFactory {
    public ColumnSequence create(String beginColumn);
}
