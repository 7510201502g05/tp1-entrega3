package ar.fiuba.tdd.tp1.window.table;

import java.util.Arrays;

/**
 * Secuencia simple de columnas con letras del abecedario.
 * 
 * @author martin
 *
 */
public class SimpleColumnSequence implements ColumnSequence {
    private static final String[] COL_SEQ = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"
            .split(" ");
    private String first = COL_SEQ[0];
    private String last = COL_SEQ[COL_SEQ.length - 1];
    private int colSeqIdx = 0;
    private boolean reset = false;

    public String getFirst() {
        return first;
    }

    public String getLast() {
        return last;
    }

    @Override
    public ColumnSequence clearReset() {
        reset = false;
        return this;
    }

    @Override
    public boolean didReset(boolean clearReset) {
        boolean res = reset;
        if (clearReset) {
            this.clearReset();
        }
        return res;
    }

    public ColumnSequence doReset() {
        this.colSeqIdx = indexOfCurrentWithinSeq(first);
        reset = true;
        return this;
    }

    /**
     * Crea una secuencia a partir de una columna [A,Z].
     * 
     * @param col
     *            - Valor de columna (A-Z).
     */
    public SimpleColumnSequence(String col) {
        this(col, COL_SEQ[0], COL_SEQ[COL_SEQ.length - 1]);
    }

    /**
     * Crea una secuencia a partir de una columna y los valores limite de la secuencia.
     * 
     * @param first
     *            - Primer elemento de secuencia.
     * @param last
     *            - Ultimo elemento de secuencia.
     */
    public SimpleColumnSequence(String first, String last) {
        super();
        this.first = first.trim().toUpperCase();
        this.last = last.trim().toUpperCase();
        this.colSeqIdx = indexOfCurrentWithinSeq(first);
    }

    /**
     * Crea una secuencia a partir de una columna y los valores limite de la secuencia.
     * 
     * @param col
     *            - Columna inicial [first,last].
     * @param first
     *            - Primer elemento de secuencia.
     * @param last
     *            - Ultimo elemento de secuencia.
     */
    public SimpleColumnSequence(String col, String first, String last) {
        super();
        this.first = first.trim().toUpperCase();
        this.last = last.trim().toUpperCase();
        this.colSeqIdx = indexOfCurrentWithinSeq(col.trim().toUpperCase());
    }

    private int indexOfCurrentWithinSeq(String str) {
        return Arrays.asList(COL_SEQ).indexOf(str);
    }

    private boolean onEndOfSeq() {
        return getCurrent().equals(lastOfSeq());
    }

    private String lastOfSeq() {
        return last;
    }

    @Override
    public String getCurrent() {
        return COL_SEQ[this.colSeqIdx];
    }

    @Override
    public ColumnSequence next() {
        if (this.onEndOfSeq()) {
            this.doReset();
        } else {
            this.colSeqIdx++;
        }

        return this;
    }
}
