package ar.fiuba.tdd.tp1.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateComparator {
    private static DateComparator instance = new DateComparator();
    private static final SimpleDateFormat YYYY_MM_DD_FORMAT = new SimpleDateFormat("yyyy/MM/dd");

    private DateComparator() {
    }

    public static DateComparator compare() {
        return instance;
    }

    public static Date getAsDate(Object obj) {
        if (obj instanceof Date) {
            return (Date) obj;
        }

        if (obj instanceof Calendar) {
            return ((Calendar) obj).getTime();
        }

        if (obj instanceof Number) {
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(((Number) obj).longValue());
            return getAsDate(calendar);
        }

        throw new RuntimeException("Imposible tranformar " + obj + " en Date!");
    }

    public boolean yearMonthDay(Object first, Object second) {
        Date firstAsDate = getAsDate(first);
        Date secondAsDate = getAsDate(second);

        String strFirstAsDate = YYYY_MM_DD_FORMAT.format(firstAsDate);
        String strSecondAsDate = YYYY_MM_DD_FORMAT.format(secondAsDate);

        return strFirstAsDate.equals(strSecondAsDate);
    }

    public static void main(String[] args) {
        System.out.println(new Integer(12) instanceof Number);
        System.out.println(new GregorianCalendar() instanceof Calendar);
    }
}
