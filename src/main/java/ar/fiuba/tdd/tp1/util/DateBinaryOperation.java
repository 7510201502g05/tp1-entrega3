package ar.fiuba.tdd.tp1.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateBinaryOperation {
    private DoubleBinaryOperation doubleBinaryOperation;
    
    public static DateBinaryOperation create(){
        return new DateBinaryOperation();
    }

//    public DateBinaryOperation(DoubleBinaryOperation doubleBinaryOperation) {
//        super();
//        this.doubleBinaryOperation = doubleBinaryOperation;
//    }
    
    private DateBinaryOperation(){
    }
    
    public DateBinaryOperation withOperation(DoubleBinaryOperation doubleBinaryOperation){
        this.doubleBinaryOperation = doubleBinaryOperation;
        return this;
    }

    public static boolean anyDate(Object first, Object second) {
        return first instanceof Date || second instanceof Date;
    }

    public static Double getAsDouble(Object obj) {
        if (obj instanceof Date) {
            Long time = ((Date) obj).getTime();
            return time.doubleValue();
        }

        if (obj instanceof Calendar) {
            return (double) ((Calendar) obj).getTimeInMillis();
        }

        return ((Number) obj).doubleValue();
    }

    public Date operate(Object first, Object second) {
        Double firstDouble = getAsDouble(first);
        Double secondDouble = getAsDouble(second);

        Double resultDouble = doubleBinaryOperation.operate(firstDouble, secondDouble);

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTimeInMillis(resultDouble.longValue());
        return gregorianCalendar.getTime();
    }
}
