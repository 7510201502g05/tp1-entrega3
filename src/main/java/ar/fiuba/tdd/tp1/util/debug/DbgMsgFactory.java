package ar.fiuba.tdd.tp1.util.debug;

public class DbgMsgFactory {
    private Integer tabs = 0;
    private static final String TAB = "  ";
    private static DbgMsgFactory msgFactory = new DbgMsgFactory();

    public static DbgMsgFactory instance() {
        return msgFactory;
    }

    protected DbgMsgFactory() {

    }

    public String createMsg(Object author, String subMsg) {
        String prefix = getPrefix(author);
        return prefix + subMsg;
    }

    private String getPrefix(Object author) {
        String tabPrefix = getTabPrefix();
        String authorName = getAuthorName(author);
        String prefix = tabPrefix + authorName + "::";
        return prefix;
    }

    private String getTabPrefix() {
        String tabPrefix = "";
        for (int i = 0; i < tabs; i++) {
            tabPrefix += TAB;
        }
        return tabPrefix;
    }

    private String getAuthorName(Object author) {
        String authorName = (author == null || author.getClass().getSimpleName().isEmpty()) ? "()->{}" : author.getClass().getSimpleName();
        return authorName;
    }

    public String createMethodCallMsg(Object author, String methodName, Object... args) {
        String message = "";
        String prefix = getPrefix(author) + methodName;
        message += prefix + "(";
        if (args == null || args.length == 0) {
            return message + ")";
        }
        int lastIndex = args.length - 1;
        for (int i = 0; i < args.length; i++) {
            Object object = args[i];
            message += object.toString();
            if (i < lastIndex) {
                message += ",";
            }
        }

        return message + ")";
    }

    public DbgMsgFactory augmentTab() {
        ++tabs;
        return this;
    }

    public DbgMsgFactory reduceTab() {
        --tabs;
        tabs = tabs < 0 ? 0 : tabs;
        return this;
    }

}
