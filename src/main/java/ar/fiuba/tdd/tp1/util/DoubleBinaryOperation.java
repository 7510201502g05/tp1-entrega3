package ar.fiuba.tdd.tp1.util;

@FunctionalInterface
public interface DoubleBinaryOperation {
    public Double operate(Double first, Double second);

    public static Double addition(Double first, Double second) {
        return first + second;
    }

    public static Double subtraction(Double first, Double second) {
        return first - second;
    }
}
