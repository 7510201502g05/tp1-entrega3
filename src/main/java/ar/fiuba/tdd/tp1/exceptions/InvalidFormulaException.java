package ar.fiuba.tdd.tp1.exceptions;

/**
 * Created by Leandro on 03-Oct-15. :)
 */
public class InvalidFormulaException extends Exception {
    @Override
    public String getMessage() {
        return "The formula is not valid";
    }
}
