package ar.fiuba.tdd.tp1.exceptions;

/**
 * Created by Leandro on 24-Oct-15. :)
 */
public class InvalidCellReferenceException extends Exception {
    @Override
    public String getMessage() {
        return "There is a cyclic reference between cells";
    }
}
