package ar.fiuba.tdd.tp1.exceptions;

/*
 * Usada cuando en la Celda no hay ningun valor para devolver
 */
public class NoValueException extends Exception {
    @Override
    public String getMessage() {
        return "There is no Value";
    }
}
