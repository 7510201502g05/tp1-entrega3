package ar.fiuba.tdd.tp1.exceptions;

/**
 * Created by Leandro on 19-Oct-15. :)
 */
public class InvalidFormatException extends Exception {
    @Override
    public String getMessage() {
        return "The format is not valid";
    }
}
