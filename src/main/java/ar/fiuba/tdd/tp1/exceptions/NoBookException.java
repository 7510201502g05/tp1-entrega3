package ar.fiuba.tdd.tp1.exceptions;

/**
 * Created by ale on 10/5/15.
 */
public class NoBookException extends Exception {
    @Override
    public String getMessage() {
        return "There is no such Book";
    }
}
