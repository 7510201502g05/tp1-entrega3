package ar.fiuba.tdd.tp1.exceptions;

public class InsufficientArgumentsOnPrint extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InsufficientArgumentsOnPrint(String msg) {
        super(msg);
    }
    
}
