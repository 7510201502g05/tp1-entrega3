package ar.fiuba.tdd.tp1.parsers.operation;

import ar.fiuba.tdd.tp1.util.DateBinaryOperation;
import ar.fiuba.tdd.tp1.util.DoubleBinaryOperation;

public abstract class ArithmeticOperation implements Operation {
    protected DoubleBinaryOperation doubleBinaryOperation;
    
    public abstract DoubleBinaryOperation getDoubleBinaryOperation();

    @Override
    public Object operate(Object... arguments) {
        DoubleBinaryOperation doubleBinaryOperation = getDoubleBinaryOperation();
        
        Object firstOperand = arguments[0];
        Object secondOperand = arguments[1];

        if (DateBinaryOperation.anyDate(firstOperand, secondOperand)) {
            return DateBinaryOperation.create().withOperation(doubleBinaryOperation)
                    .operate(firstOperand, secondOperand);
        }

        return doubleBinaryOperation.operate(((Number) firstOperand).doubleValue(),
                ((Number) secondOperand).doubleValue());
    }
}
