package ar.fiuba.tdd.tp1.parsers.operation;

import java.util.GregorianCalendar;

public class TodayOperation implements Operation {
    @Override
    public Object operate(Object... arguments) {
        return new GregorianCalendar().getTime();
    }

    @Override
    public String toString() {
        return "TodayOperation";
    }

    @Override
    public boolean nullableArguments() {
        return true;
    }
}
