package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.parsers.operation.Operation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Leandro on 18-Oct-15. :)
 */
public class SetOperationCollaborativeParser extends OperationCollaborativeParser {
    /**
     * SetOperationCollaborativeParser constructor
     *
     * @param symbol
     *            a Symbol of the operand used to parse
     * @param operation
     *            an operation to apply to the operands
     * @param parser
     *            a parser to continue.
     */
    public SetOperationCollaborativeParser(String symbol, Operation operation, FormulaParser parser) {
        super(symbol, operation, parser);
    }

    /**
     * Get the set of expressions that this parser cant parse in the given expression.
     *
     * @param expression
     *            representation to parse.
     * @return a set of expressions that this parser cant parse.
     */
    public List<String> getSubExpressions(final String expression) {
        /*
         * establezco que si se intenta obtener las sub expresiones de
         * expression y no hay coincidencia de funcion (ej: intentar parsear MAX(A3:B4) cuando
         * getSymbolToParse() es CONCAT) entonces se devuelve una lista de simbolos de un solo
         * elemento como si nada hubiese pasado. Ejemplo: parsear MAX(A1:B3) usando expresion
         * regular de CONCAT, retornaria [MAX(A1:B3)]...
         */
        if (!expression.contains(getSymbolToParse())) {
            print(expression + " does not contain " + getSymbolToParse());
            List<String> exprList = new ArrayList<String>();
            exprList.add(expression);
            return exprList;
        }

        /*
         * divido la expresion por el simbolo para obtener sub expresiones a parsear con otro parser
         * distinto a este.
         */
        //        String regularExpressionToParse = getRegularExpressionToParse();
        //        print("regularExpressionToParse=" + regularExpressionToParse);
        //        List<String> subExpressions = new ArrayList<String>(Arrays.asList(expression
        //                .split(regularExpressionToParse)));
        //
        //        return preserveSymbol(subExpressions);

        return super.getSubExpressions(expression);
    }

    @Override
    public String getRegularExpressionToParse() {
        return Pattern.quote(getSymbolToParse() + "(") + "|" + Pattern.quote(",") + "|"
                + Pattern.quote(")");
    }

    @Override
    public List<String> preserveSymbol(List<String> subExpressions) {
        //        subExpressions.remove("");
        //        subExpressions.remove(" ");
        subExpressions.removeIf(sym -> sym.trim().isEmpty());
        return subExpressions;
    }
}
