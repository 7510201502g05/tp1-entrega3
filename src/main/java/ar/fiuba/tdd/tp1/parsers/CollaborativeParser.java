package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.model.formulas.Formula;
import ar.fiuba.tdd.tp1.model.formulas.InvalidFormula;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Pattern;

/**
 * Created by Leandro on 03-Oct-15. :)
 */
public abstract class CollaborativeParser implements FormulaParser {

    private FormulaParser nextParserToCall;

    /**
     * CollaborativeParser constructor.
     *
     * @param parser
     *            a FormulaParser instance.
     */
    public CollaborativeParser(final FormulaParser parser) {
        nextParserToCall = parser;
    }

    /**
     * Parse the given expression by a collaborative approach.
     *
     * @param expression
     *            the representation of a Formula.
     * @return the Formula represented by the expression.
     */
    public Formula collaborativeParse(final String expression) {
        return nextParserToCall.parse(expression);
    }

    /**
     * Get the Formulas for the given set of expressions.
     *
     * @param subExpressions
     *            a set of representations of formulas
     * @return a set of formulas represented by the set of expressions
     */
    public List<Formula> getSubFormulas(final List<String> subExpressions) {
        List<Formula> subFormulas = new ArrayList<>();
        //parseo las sub expresiones y obtengo las formulas que resultan.
        for (String subExpression : subExpressions) {
            subFormulas.add(collaborativeParse(subExpression));
        }
        return subFormulas;
    }

    public abstract String getSymbolToParse();

    public String getRegularExpressionToParse() {
        return Pattern.quote(getSymbolToParse());
    }

    /**
     * Corrects the parse of the set of expressions in case that a symbol is missing.
     *
     * @param subExpressions
     *            a set of representations (which may be incorrect) of formulas.
     * @return the correct set of expressions
     */
    public List<String> preserveSymbol(List<String> subExpressions) {
        ListIterator<String> iterator = subExpressions.listIterator();
        while (iterator.hasNext()) {
            String currentSubExpression = iterator.next();
            if (currentSubExpression.equals("") || currentSubExpression.equals(" ")) {
                String termWithSign = iterator.next();
                iterator.set(getSymbolToParse() + termWithSign);
            }
        }
//        subExpressions.remove("");
//        subExpressions.remove(" ");
        subExpressions.removeIf(sym -> sym.trim().isEmpty());
        return subExpressions;
    }

    /**
     * Get the set of expressions that this parser cant parse in the given expression.
     *
     * @param expression
     *            representation to parse.
     * @return a set of expressions that this parser cant parse.
     */
    public List<String> getSubExpressions(final String expression) {
        /*
         * divido la expresion por el simbolo para obtener sub expresiones a parsear con otro parser
         * distinto a este.
         */
        String regularExpressionToParse = getRegularExpressionToParse();
        print("regularExpressionToParse=" + regularExpressionToParse);
        List<String> subExpressions = new ArrayList<String>(Arrays.asList(expression
                .split(regularExpressionToParse)));

        return preserveSymbol(subExpressions);
    }

    /**
     * Make a Formula composed of an operation between the given set of Formulas.
     *
     * @param subFormulas
     *            a set of Formulas that will be used as operands of a Formula
     * @return a Formula composed of an operation between the given set of Formulas.
     */
    public Formula makeFormula(final List<Formula> subFormulas) {
        return subFormulas.get(0);
    }

    @Override
    public Formula parse(final String expression) {
        printMethodCall("parse", expression);
        List<Formula> subFormulas;
        try {
            List<String> subExpressions = getSubExpressions(expression);
            print("subExpressions=" + subExpressions);
            augmentTab();
            subFormulas = getSubFormulas(subExpressions);
            reduceTab();
        } catch (Exception e) {
            return new InvalidFormula();
        }
        return makeFormula(subFormulas);
    }

}
