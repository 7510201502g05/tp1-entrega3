package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellReferenceException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormulaException;
import ar.fiuba.tdd.tp1.model.formulas.Formula;
import ar.fiuba.tdd.tp1.parsers.operation.Operation;
import ar.fiuba.tdd.tp1.parsers.operation.PrintOperation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;

/**
 * Created by Leandro on 03-Oct-15. :)
 */
public class OperationCollaborativeParser extends CollaborativeParser {

    private String symbolToParse;
    private Operation operationToAplly;

    /**
     * OperationCollaborativeParser constructor
     *
     * @param symbol
     *            a Symbol of the operand used to parse
     * @param operation
     *            an operation to apply to the operands
     * @param parser
     *            a parser to continue.
     */
    public OperationCollaborativeParser(final String symbol, final Operation operation,
            final FormulaParser parser) {
        super(parser);
        symbolToParse = symbol;
        operationToAplly = operation;
    }

    public String getSymbolToParse() {
        return symbolToParse;
    }

    public Formula makeFormula(final List<Formula> subFormulas) {
        return new OperationFormula(subFormulas);
    }

    public class OperationFormula implements Formula {
        private final List<Formula> subFormulas;

        public OperationFormula(List<Formula> subFormulas) {
            super();
            this.subFormulas = subFormulas;
        }

        @Override
        public Object eval() throws InvalidFormulaException, InvalidFormatException,
                InvalidCellReferenceException {
            try {
                if (operationToAplly instanceof PrintOperation) {
                    try {
                        return invokePrintOperation();
                    } catch (Exception e) {
                    }
                }
                
                /*
                 * evaluo la formula entre el primer operando (sub formula) y todos los restantes
                 */
                ListIterator<Formula> iterator = subFormulas.listIterator();
                
                Formula formula = iterator.hasNext() ? iterator.next() : null;
                Object result = formula == null ? operationToAplly.operate(null, null) : formula.eval();
                
                while (iterator.hasNext()) {
                    Object arg = iterator.next().eval();
                    result = operationToAplly.operate(result, arg);
                }
                return result;
            } catch (InvalidCellReferenceException e2) {
                throw new InvalidCellReferenceException();
            } catch (InvalidFormatException e) {
                throw new InvalidFormatException();
            } catch (Exception e) {
                throw new InvalidFormulaException();
            }

        }

        private Object invokePrintOperation() throws InvalidFormulaException, InvalidFormatException, InvalidCellReferenceException {
            List<Object> arguments = new ArrayList<>();
            
            for (Formula formula : subFormulas) {
                Object nextArg = formula.eval();
                arguments.add(nextArg);
            }
            
            Object result = operationToAplly.operate(arguments.toArray());
            return result;
        }

        @Override
        public String toString() {
            return "OperationFormula{subFormulas=" + subFormulas + "}";
        }

    }

    public static void main(String[] args) {
        String[] split = "hola".split("\\.");
        System.out.println(Arrays.asList(split));
    }
}
