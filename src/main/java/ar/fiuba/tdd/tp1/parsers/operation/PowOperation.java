package ar.fiuba.tdd.tp1.parsers.operation;

public class PowOperation implements Operation {
    @SuppressWarnings("CPD-START")
    @Override
    public Object operate(Object... args) {
        return Math.pow(((Number) args[0]).doubleValue(), ((Number) args[1]).doubleValue());
    }

    @SuppressWarnings("CPD-END")
    @Override
    public String toString() {
        return "PowOperation";
    }

}
