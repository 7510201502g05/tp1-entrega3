package ar.fiuba.tdd.tp1.parsers.operation;

import ar.fiuba.tdd.tp1.util.DoubleBinaryOperation;

public class AdditionOperation extends ArithmeticOperation {
    @Override
    public String toString() {
        return "AdditionOperation";
    }

    @Override
    public DoubleBinaryOperation getDoubleBinaryOperation() {
        return DoubleBinaryOperation::addition;
    }
}
