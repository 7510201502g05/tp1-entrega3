package ar.fiuba.tdd.tp1.parsers.operation;

public abstract class ShortenOperation implements Operation {
    protected String getFirstOperand(Object first) {
        return first.toString();
    }

    protected Integer getSecondOperand(Object second) {
        return ((Number) second).intValue();
    }

    @Override
    public Object operate(Object... arguments) {
        String firstOperand = getFirstOperand(arguments[0]);
        Integer secondOperand = getSecondOperand(arguments[1]);
        
        if (secondOperand >= firstOperand.length()) {
            return firstOperand;
        }

        return solveOperate(firstOperand,secondOperand);
    }

    protected abstract Object solveOperate(String firstOperand, Integer secondOperand);
}
