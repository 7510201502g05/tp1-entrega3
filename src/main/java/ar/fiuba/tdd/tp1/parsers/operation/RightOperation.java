package ar.fiuba.tdd.tp1.parsers.operation;

public class RightOperation extends ShortenOperation {
    @Override
    public String toString() {
        return "RightOperation";
    }

    @Override
    protected Object solveOperate(String firstOperand, Integer secondOperand) {
        StringBuilder reverseBuilder = new StringBuilder(new StringBuilder(firstOperand).reverse()
                .substring(0, secondOperand)).reverse();

        return reverseBuilder.toString();
    }
}
