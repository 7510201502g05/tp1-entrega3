package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.parsers.operation.Operation;

public class TodayCollaborativeParser extends SetOperationCollaborativeParser {

    public TodayCollaborativeParser(String symbol, Operation operation, FormulaParser parser) {
        super(symbol, operation, parser);
    }

    @Override
    public String getRegularExpressionToParse() {
        return "TODAY\\(\\)";
    }
}
