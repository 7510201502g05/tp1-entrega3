package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.model.Book;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Esta clase se encarga de manejar las operaciones con nombres de rangos.
 * 
 * @author Galli
 *
 */
public class RangeNameOperationCollaborativeParser {

    private Book book;

    public RangeNameOperationCollaborativeParser(Book book) {
        this.book = book;
    }

    /**
     * Devuelve la expression pasada por parametros con los rangos que corresponden.
     * 
     * @param expression
     *            : expression que se quiere analizar.
     * @return la expression con los rangos correspondientes.
     */
    public String getExpressions(final String expression) {
        List<String> formula = new ArrayList<>();
        if (expression.contains("!")) {
            replaceRanges(expression, formula);
            return returnSubExpressions(formula);
        }
        return expression;
    }

    /**
     * Analiza la expression, palabra por palabra y devuelve una lista de las subexpressiones
     * analizadas.
     * 
     * @param expression
     *            : es la expression a analizar.
     * @param subExpressions
     *            : lista de subexpressiones analizadas.
     */
    private void replaceRanges(String expression, List<String> subExpressions) {
        // NADIA GALLI: Si no hay espacios todo explota
        // expression=expression.replace(" ", "");
        if ((!expression.contains(")"))
                || ((expression.contains(")")) && (expression.length() <= 1))) {
            subExpressions.add(expression);
            return;
        }
        String[] rest = expression.split(Pattern.quote(")"));
        String firstString = rest[0];
        if (firstString.equals("")) {
            fistStringIsClosingParenthesis(subExpressions, rest, expression);
        } else {
            String[] subFirstString = firstString.split(Pattern.quote("("));
            if (subFirstString[0].length() <= 1) {
                subExpressions.add("(");
                removeSubstringInString("(", expression, subExpressions);
                return;
            }
            firstString = subFirstString[0];
            firstString = getFirstStringToParse(rest, firstString);
            evaluateFirstStringToParse(firstString, expression, subExpressions);
        }
    }

    /**
     * Evalua la primera palabra. Si es un nombre de rango hace el reemplazo correspondiente y si no
     * lo es sigue con la siguiente palabra.
     * 
     * @param firstString
     *            : palabra a analizar.
     * @param expression
     *            : expression que se esta analizando.
     * @param subExpressions
     *            : lista de subexpressiones analizadas.
     */
    private void evaluateFirstStringToParse(String firstString, String expression,
            List<String> subExpressions) {
        if (!firstString.matches("^!.+")) {
            final String firstExpression = firstString.split("!")[0];
            subExpressions.add(firstExpression);
            removeSubstringInString(firstExpression, expression, subExpressions);
            return;
        }
        this.replaceRange(firstString, expression, subExpressions);
    }

    /**
     * La primera palabra es un ")" enotnces revisa si hay mas expresiones a analizar y las analiza
     * sino termina.
     * 
     * @param subExpressions
     *            : lista de subexpressiones analizadas.
     * @param rest
     *            : es la expression spliteada con ")".
     * @param expression
     *            : expression que se esta analizando.
     */
    private void fistStringIsClosingParenthesis(List<String> subExpressions, String[] rest,
            String expression) {
        subExpressions.add(")");
        if (rest.length > 1) {
            removeSubstringInString(")", expression, subExpressions);
        }
    }

    /**
     * Busca la primera palabra a analizar.
     * 
     * @param rest
     *            : es el conjunto de palabras de una expression spliteada con ")".
     * @param firstString
     *            : es la primera palabra hasta el momento.
     * @return la primera palabra a analizar, si habia "," toma la primera palabra sino devuelve la
     *         primera palabra que se tenia hasta el momento.
     */
    private String getFirstStringToParse(String[] rest, String firstString) {
        if (rest.length > 1) {
            if (rest[0].contains(",")) {
                String[] subRest = rest[0].split(",");
                return subRest[0];
            }
        }
        return firstString;
    }

    /**
     * Elimina la firstExpression analizada de la expression y analiza la siguiente palabra de la
     * expression.
     * 
     * @param firstExpression
     *            : palabra analizada de la expression.
     * @param expression
     *            : expression que se esta analizando.
     * @param subExpressions
     *            : expressiones ya analizadas de la expression.
     */
    private void removeSubstringInString(String firstExpression, String expression,
            List<String> subExpressions) {
        String newExpression = expression.replaceFirst(Pattern.quote(firstExpression), "");
        // System.out.println("Substring que no modificamos: " + newExpression);
        this.replaceRanges(newExpression, subExpressions);
    }

    /**
     * Si la primera palabra (firstString) es un rango entonces guarda el valor del rango en la
     * lista de subExpression, sino guarda la palabra y sigue.
     * 
     * @param firstString
     *            : palabra que se va a analizar.
     * @param expression
     *            : expresion que se esta analizando.
     * @param subExpressions
     *            : lista de subExpressions analizadas.
     */
    private void replaceRange(String firstString, String expression, List<String> subExpressions) {
        if (firstString.equals("")) {
            return;
        }
        // System.out.println("firstString " + firstString);
        String[] range = firstString.split("!");
        range = range[1].split("[.]");
        if (range.length == 2) {
            solveIfRange(range, expression, firstString, subExpressions);
        } else {
            solveIfNotRange(range[1], expression, firstString, subExpressions);
        }
    }

    /**
     * Obtiene el rango que tiene las especificaciones de range y sigue con la siguiente palabra.
     * 
     * @param range
     *            : composicion del nombre de rango [ nombre de hoja , nombre de rango ]
     * @param expression
     *            : expresion que se esta analizando.
     * @param firstString
     *            : nombre de rango compuesto (!nombreHoja.nombreRango).
     * @param subExpressions
     *            : lista de subExpressions analizadas.
     */
    private void solveIfRange(String[] range, String expression, String firstString,
            List<String> subExpressions) {
        String rangeMod = book.getRange(range[0], range[1]);
        if (!rangeMod.equals("")) { // Si encuentra un rango
            subExpressions.add(rangeMod);
            removeSubstringInString(firstString, expression, subExpressions);
            return;
        }
    }

    /**
     * La primera palabra a analizar no es un nombre de rango, sigue con la siguiente palabra.
     * 
     * @param subExpression
     *            : palabra que tiene un ! y no es un nombre de rango.
     * @param expression
     *            : expresion que se esta analizando.
     * @param firstString
     *            : palabra que no es un nombre de rango.
     * @param subExpressions
     *            : lista de subExpressions analizadas.
     */
    private void solveIfNotRange(String subExpression, String expression, String firstString,
            List<String> subExpressions) {
        if (subExpression.contains("!")) {
            removeSubstringInString("!" + subExpression, expression, subExpressions);
        }
        subExpressions.add(firstString);
    }

    /**
     * Devuelve un string que es la union de las subExpressions.
     * 
     * @param subExpressions
     *            : lista de expresiones que se quieren unir.
     * @return un string con las expresiones unidas.
     */
    private String returnSubExpressions(List<String> subExpressions) {
        final StringBuilder stringBuilder = new StringBuilder();
        System.out.println("Lista de formuala: " + subExpressions.toString());
        subExpressions.forEach(s -> stringBuilder.append(s));
        return stringBuilder.toString();
    }

}
