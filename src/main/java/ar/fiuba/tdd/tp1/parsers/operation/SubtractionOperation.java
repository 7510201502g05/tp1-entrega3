package ar.fiuba.tdd.tp1.parsers.operation;

import ar.fiuba.tdd.tp1.util.DoubleBinaryOperation;

public class SubtractionOperation extends ArithmeticOperation {
    @Override
    public String toString() {
        return "SubtractionOperation";
    }

    @Override
    public DoubleBinaryOperation getDoubleBinaryOperation() {
        return DoubleBinaryOperation::subtraction;
    }
}
