package ar.fiuba.tdd.tp1.parsers.operation;

public class MaxOperation implements Operation {
    @SuppressWarnings("CPD-START")
    @Override
    public Object operate(Object... args) {
        return Math.max(((Number) args[0]).doubleValue(), ((Number) args[1]).doubleValue());
    }

    @SuppressWarnings("CPD-END")
    @Override
    public String toString() {
        return "MaxOperation";
    }

}
