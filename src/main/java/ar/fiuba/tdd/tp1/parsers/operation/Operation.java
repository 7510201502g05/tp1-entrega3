package ar.fiuba.tdd.tp1.parsers.operation;

/**
 * Created by Leandro on 03-Oct-15. :)
 */
public interface Operation {
    /**
     * Calculates the result of the operation betwen the first and second operands.
     *
     * @param firstOperand
     *            the first operand
     * @param secondOperand
     *            the second operand
     * @return the result as a Double.
     */
    Object operate(Object... arguments);

    default boolean nullableArguments() {
        return false;
    }
}
