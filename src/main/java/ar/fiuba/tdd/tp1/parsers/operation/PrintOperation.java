package ar.fiuba.tdd.tp1.parsers.operation;

import ar.fiuba.tdd.tp1.exceptions.InsufficientArgumentsOnPrint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrintOperation implements Operation {

    @Override
    public Object operate(Object... arguments) {
        String outputResult = (String) arguments[0];
        Map<String, String> argumentsMap = new HashMap<String, String>();
        List<String> sentenceReferences = this.getReferencesOnSentence(outputResult);

        createArgumentMap(argumentsMap, arguments);

        if (this.insufficientReferencesOnMap(sentenceReferences, argumentsMap)) {
            throw new InsufficientArgumentsOnPrint(
                    "La cantidad de argumentos pasados son menos que sus llamados");
        }

        for (String parameter : argumentsMap.keySet()) {
            outputResult = outputResult.replaceAll("\\" + parameter, argumentsMap.get(parameter));
        }
        
        outputResult = outputResult.replaceAll("\\&\\$", "\\$");

        return outputResult;
    }

    private void createArgumentMap(Map<String, String> argumentsMap, Object... arguments) {
        // armo un mapa con los argumentos
        for (int i = 1; i < arguments.length; i++) {
            String mapKey = "$" + String.valueOf(i);
            argumentsMap.put(mapKey, arguments[i].toString());
        }
    }

    private boolean insufficientReferencesOnMap(List<String> sentenceReferences,
            Map<String, String> argumentsMap) {
        boolean result = false;
        for (String reference : sentenceReferences) {
            if (argumentsMap.get(reference) == null) {
                return true;
            }
        }
        return result;
    }

    private List<String> getReferencesOnSentence(String sentence) {
        String[] ocurrences = sentence.split("\\$");
        List<String> references = new ArrayList<String>();
        boolean literalFlag = false;
        
        for (String ocurrence : ocurrences) {
            if(!ocurrence.isEmpty()){
                if (ocurrence.substring(0, 1).matches("[-+]?\\d*\\.?\\d+") && !literalFlag) {
                    String reference = "$" + ocurrence.substring(0, 1);
                    if (!references.contains(reference)) {
                        references.add(reference);
                    }
                }
                
                String flagcheck = ocurrence.substring(ocurrence.length() - 1);
                
                if(flagcheck.contains("&")){
                    literalFlag = true;
                }else{
                    literalFlag = false;
                }
            }
        }

        return references;
    }

    @Override
    public String toString() {
        return "PrintOperation";
    }

}
