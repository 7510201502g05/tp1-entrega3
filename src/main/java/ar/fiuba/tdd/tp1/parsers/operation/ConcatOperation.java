package ar.fiuba.tdd.tp1.parsers.operation;

public class ConcatOperation implements Operation {

    @Override
    public Object operate(Object... arguments) {
        Object firstOperand = arguments[0];
        Object secondOperand = arguments[1];

        return firstOperand.toString() + secondOperand.toString();
    }

    @Override
    public String toString() {
        return "ConcatOperation";
    }

}
