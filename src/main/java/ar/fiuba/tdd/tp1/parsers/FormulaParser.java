package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.model.formulas.Formula;
import ar.fiuba.tdd.tp1.util.debug.DbgMsgFactory;

/**
 * Created by Leandro on 03-Oct-15. :)
 */
public interface FormulaParser {

    /**
     * parsea una expresion para constituir una estructura de formulas
     * contenidas en dicha expresion.
     *
     * @param expression
     *            an expression of a Formula to parse
     * @return the Formula instance of the parsed expression.
     */
    Formula parse(final String expression);

    default void print(String msg) {
        String message = DbgMsgFactory.instance().createMsg(this, msg);
        System.out.println(message);
    }

    default void augmentTab() {
        DbgMsgFactory.instance().augmentTab();
    }

    default void reduceTab() {
        DbgMsgFactory.instance().reduceTab();
    }

    default void printMethodCall(String methodName, Object... args) {
        String message = DbgMsgFactory.instance().createMethodCallMsg(this, methodName, args);
        System.out.println(message);
    }
}
