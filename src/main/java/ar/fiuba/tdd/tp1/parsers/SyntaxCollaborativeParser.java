package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.formulas.Formula;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Leandro on 17-Oct-15. :)
 */

/*
 * Clase para el parseo de formulas evaluables, reconocibles por primer caracter '=' .
 */

public class SyntaxCollaborativeParser extends CollaborativeParser {

    private CollaborativeParser valueParser;
    private Book book;

    public SyntaxCollaborativeParser(Book book, OperationCollaborativeParser operationsParser,
            CollaborativeParser valueParser) {
        super(operationsParser);
        this.valueParser = valueParser;
        this.book = book;
    }

    @Override
    public String getSymbolToParse() {
        return "=";
    }

    @Override
    public List<Formula> getSubFormulas(final List<String> subExpressions) {
        /**
         * Si la expresion correspondia a una formula por tener el =, entonces se parsean las
         * operaciones, sino unicamente se parsean los valores.
         */
        List<Formula> subFormulas = new ArrayList<>();
        if (subExpressions.size() > 1) {
            RangeNameOperationCollaborativeParser operation = new RangeNameOperationCollaborativeParser(
                    book);
            //Reemplaza los nombre de rangos por los rangos correspondientes
            //System.out.println("String que se pasa al namerange: " + subExpressions.get(1));
            String subExpr = operation.getExpressions(subExpressions.get(1));
            //System.out.println("Expression con rangos verdaderos: " + subExpr);
            Formula collaborativeParsed = collaborativeParse(subExpr);
            subFormulas.add(collaborativeParsed);
        } else {
            /*
             * si subExpressions.size es 1 -> el contenido a parsear es un VALOR, por lo tanto se
             * intentara parsearlo con los parsers de valor
             */
            String subExpr = subExpressions.get(0);
            Formula valueParsed = valueParser.parse(subExpr);
            subFormulas.add(valueParsed);
        }
        return subFormulas;
    }

    public List<String> preserveSymbol(List<String> subExpressions) {
        return subExpressions;
    }

}
