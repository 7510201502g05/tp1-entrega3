package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.model.formulas.Formula;
import ar.fiuba.tdd.tp1.model.formulas.ValueFormula;

/**
 * Created by Leandro on 03-Oct-15. :)
 */
public class ValueParser implements FormulaParser {
    @Override
    public Formula parse(final String expression) {
        try {
            printMethodCall("parse", expression);
            print("trying to parse <" + expression + "> as number...");

            if (expression.contains(".")) {
                return new ValueFormula(Double.parseDouble(expression.replace(" ", "")));
            } else {
                return new ValueFormula(Integer.parseInt(expression.replace(" ", "")));
            }
        } catch (NumberFormatException e) {
            /* El valor no es un Double ni un entero -> lo ingreso como Object puro */
            print("faild to parse <" + expression + "> as number. Interpreting as object...");
            return new ValueFormula(expression);
        }

    }
}
