package ar.fiuba.tdd.tp1.parsers.operation;

public class LeftOperation extends ShortenOperation {
    @Override
    public String toString() {
        return "LeftOperation";
    }

    @Override
    protected Object solveOperate(String firstOperand, Integer secondOperand) {
        return firstOperand.substring(0, secondOperand);
    }
}
