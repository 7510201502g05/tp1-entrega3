package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.formulas.Formula;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Leandro on 04-Oct-15. :)
 */
public class ReferenceCollaborativeParser extends CollaborativeParser {

    private Book book;
    private String symbolToParse;

    /**
     * ReferenceCollaborativeParser constructor.
     *
     * @param parser a parser to continue
     * @param book   a Book that contains the different Sheets used to referenciate diferent cells
     */
    public ReferenceCollaborativeParser(final String symbol,
                                        final FormulaParser parser,
                                        Book book) {
        super(parser);
        symbolToParse = symbol;
        this.book = book;
    }

    @Override
    public String getSymbolToParse() {
        return symbolToParse;
    }


    @Override
    public List<Formula> getSubFormulas(final List<String> subExpressions) {
        List<Formula> subFormulas = new ArrayList<>();
        try {
            if (subExpressions.size() > 1) {
                subFormulas.add(book.getReferenceOf(
                        subExpressions.get(0).replace(" ", "").replace("!", ""), subExpressions.get(1).replace(" ", "")));
            } else {
                subFormulas.add(book.getReferenceOf(book.getWorkingSheet(), subExpressions.get(0).replace(" ", "")));
            }
            /*catch (RuntimeException e) {
            throw e;
        }*/
        } catch (Exception e) {
            if (subExpressions.size() > 1) {
                subFormulas.add(collaborativeParse(subExpressions.get(0) + getSymbolToParse() + subExpressions.get(1)));
            } else {
                subFormulas.add(collaborativeParse(subExpressions.get(0)));
            }
        }
        return subFormulas;
    }


}
