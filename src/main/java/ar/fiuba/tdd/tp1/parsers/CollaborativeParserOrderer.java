package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.formulas.Formula;
import ar.fiuba.tdd.tp1.parsers.operation.AdditionOperation;
import ar.fiuba.tdd.tp1.parsers.operation.ConcatOperation;
import ar.fiuba.tdd.tp1.parsers.operation.LeftOperation;
import ar.fiuba.tdd.tp1.parsers.operation.MaxOperation;
import ar.fiuba.tdd.tp1.parsers.operation.MinOperation;
import ar.fiuba.tdd.tp1.parsers.operation.Operation;
import ar.fiuba.tdd.tp1.parsers.operation.PowOperation;
import ar.fiuba.tdd.tp1.parsers.operation.PrintOperation;
import ar.fiuba.tdd.tp1.parsers.operation.RightOperation;
import ar.fiuba.tdd.tp1.parsers.operation.SubtractionOperation;
import ar.fiuba.tdd.tp1.parsers.operation.TodayOperation;

import java.util.List;

/**
 * Created by Leandro on 03-Oct-15. :)
 */
public class CollaborativeParserOrderer {
    /**
     * CollaborativeParserOrderer getter.
     *
     * @param book
     *            the Book to give to the ReferenceCollaborativeParser.
     * @return an OperationCollaborativeParser instance depending on the operation.
     */
    public CollaborativeParser getCollaborativeParser(Book book) {
        final Operation addition = new AdditionOperation();
        final Operation subtraction = new SubtractionOperation();
        final Operation min = new MinOperation();
        final Operation max = new MaxOperation();
        final Operation concat = new ConcatOperation();

        /* OPERACIONES AGREGADAS POR GRUPO5... */
        final Operation power = new PowOperation();
        final Operation today = new TodayOperation();
        final Operation left = new LeftOperation();
        final Operation right = new RightOperation();
        final Operation print = new PrintOperation();

        /* AGREGUE DOS FORMATOS NUEVOS DETECTABLES PARA LA LECTURA DE FECHAS... */
        CollaborativeParser valueParsers = new DateCollaborativeParser("yyyy-MM-dd'T'HH:mm:ss'Z'",
                new DateCollaborativeParser("yyyy-MM-dd", new DateCollaborativeParser("yyyy/MM/dd",
                        new ValueParser())));

        FormulaParser loQueSePasaDeLosMaxCaracteres = new RangeOperationCollaborativeParser(":",
                min, book, new SetOperationCollaborativeParser("AVERAGE", addition,
                        new RangeOperationCollaborativeParser(":", addition, book,
                                new ReferenceCollaborativeParser(".", valueParsers, book)) {
                            @Override
                            public Formula makeFormula(List<Formula> subFormulas) {
                                return () -> {
                                    if (subFormulas.size() > 1) {
                                        return ((Number) super.makeFormula(subFormulas).eval())
                                                .doubleValue() / subFormulas.size();
                                    } else {
                                        return subFormulas.get(0).eval();
                                    }
                                };
                            }
                        }));

        return new SyntaxCollaborativeParser(
                book,
                new OperationCollaborativeParser(
                        "+",
                        addition,
                        new OperationCollaborativeParser(
                                "-",
                                subtraction,
                                new OperationCollaborativeParser(
                                        "^",
                                        power,
                                        new SetOperationCollaborativeParser(
                                                "PRINT",
                                                print,
                                                new TodayCollaborativeParser(
                                                        "TODAY",
                                                        today,
                                                        new SetOperationCollaborativeParser(
                                                                "CONCAT",
                                                                concat,
                                                                new RangeOperationCollaborativeParser(
                                                                        ":",
                                                                        concat,
                                                                        book,
                                                                        new SetOperationCollaborativeParser(
                                                                                "LEFT",
                                                                                left,
                                                                                new SetOperationCollaborativeParser(
                                                                                        "RIGHT",
                                                                                        right,
                                                                                        new SetOperationCollaborativeParser(
                                                                                                "MAX",
                                                                                                max,
                                                                                                new RangeOperationCollaborativeParser(
                                                                                                        ":",
                                                                                                        max,
                                                                                                        book,
                                                                                                        new SetOperationCollaborativeParser(
                                                                                                                "MIN",
                                                                                                                min,
                                                                                                                loQueSePasaDeLosMaxCaracteres)))))))))))),
                valueParsers);
    }
}
