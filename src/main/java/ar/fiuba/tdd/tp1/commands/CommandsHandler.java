package ar.fiuba.tdd.tp1.commands;

import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.Cell;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;

/**
 * This Class is responsible of managing the doneActions and unDoneActions stacks so that
 * every Command can be undone and redone in order.
 */
public class CommandsHandler {

    Stack<Command> doneActions = new Stack<>();
    Stack<Command> unDoneActions = new Stack<>();

    public CommandsHandler() {
    }

    /**
     * Executes the command and pushes it in the doneActions Stack.
     *
     * @param toDo the Command instance to execute
     */
    public void doCommand(Command toDo) {
        toDo.execute();
        doneActions.push(toDo);
        unDoneActions.clear();//Para que ya no funcione el redo
    }

    /**
     * Undoes the las command executed.
     *
     * @throws NothingToUndoException if there is nothing to undo
     */
    public void undo() throws NothingToUndoException {
        if (doneActions.empty()) {
            throw new NothingToUndoException();
        }
        Command toUndo = doneActions.pop();
        toUndo.undo();
        unDoneActions.push(toUndo);
    }

    /**
     * Redoes the last undone Command.
     *
     * @throws NothingToRedoException if there is nothing to Redo
     */
    public void redo() throws NothingToRedoException {
        if (unDoneActions.empty()) {
            throw new NothingToRedoException();
        }
        Command toRedo = unDoneActions.pop();
        toRedo.execute();
        doneActions.push(toRedo);

    }

    /**
     * Clears done and unDone actions.
     */
    public void dropEverething() {
        doneActions = new Stack<>();
        unDoneActions = new Stack<>();
    }

    /**
     * adds a new sheet.
     *
     * @param book      the Book instance to add the Sheet
     * @param sheetName the new Sheet's name
     */
    public void addSheet(Book book, String sheetName) {
        doCommand(new AddNewSheetCommand(book, sheetName));
    }

    /**
     * Removes a Sheet.
     *
     * @param book      the Book instance that has the Sheet
     * @param sheetName the name of the Sheet instance to remove
     */
    public void removeSheet(Book book, String sheetName) {
        doCommand(new RemoveSheetCommand(book, sheetName));
    }

    /**
     * Sets a new Formula to that Cell.
     *
     * @param book    the Book instance that contains the Cell
     * @param cell    the Cell instance
     * @param formula the formula to add
     */
    public void setFormulaOf(Book book, Cell cell, String formula) {
        doCommand(new CellSetFormulaCommand(book, cell, formula));
    }
    
    public void setRange(Book book, String sheet, String name, String range) {
        doCommand(new SheetSetRangeCommand(book, sheet, name, range));
    }
    
    public void removeRange(Book book, String sheet, String name) {
        doCommand(new SheetRemoveRangeCommand(book, sheet, name));
    }

    /**
     * Adds a new Book.
     *
     * @param books    the Book Collection to add the new Book
     * @param bookName the new Book's name
     */
    public void addNewBook(Collection<Book> books, String bookName) {
        doCommand(new AddNewBookCommand(books, bookName));
    }

    /**
     * Removes a Book from the Collection.
     *
     * @param books the Collection of Books
     * @param book  the Book instance to remove
     */
    public void removeBook(ArrayList<Book> books, Book book) {
        doCommand(new RemoveBookCommand(books, book));
    }
}
