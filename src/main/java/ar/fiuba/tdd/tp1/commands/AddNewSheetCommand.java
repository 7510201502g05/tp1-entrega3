package ar.fiuba.tdd.tp1.commands;

import ar.fiuba.tdd.tp1.exceptions.NoSheetsException;
import ar.fiuba.tdd.tp1.model.Book;

/**
 * Created by ale on 10/4/15.
 */
public class AddNewSheetCommand implements Command {
    private final Book book;
    private final String sheetName;

    /**
     * AddNewSheetCommand constructor.
     *
     * @param book      the book instance to add de sheet
     * @param sheetName the name of the sheet to create and add to the @book
     */
    public AddNewSheetCommand(Book book, String sheetName) {
        this.book = book;
        this.sheetName = sheetName;
    }

    @Override
    public void execute() {
        book.addSheet(sheetName);
    }

    @Override
    public void undo() {
        try {
            book.removeLastSheet();
        } catch (NoSheetsException e) {
            e.printStackTrace();//o no hacer nada...
        }
    }
}
