package ar.fiuba.tdd.tp1.commands;

import ar.fiuba.tdd.tp1.model.Book;

/**
 * Esta clase se encarga de deshacer y rehacer la agregacion un rango en una hoja. 
 * 
 * @author Galli
 *
 */
public class SheetSetRangeCommand implements Command {
    
    private Book book;
    private String name;
    private String range;
    private String oldRange;
    private String sheet;

    public SheetSetRangeCommand(Book book, String sheet, String name, String range) {
        this.name = name;
        this.range = range;
        this.sheet = sheet;
        this.book = book;
        this.oldRange = "";
    }

    @Override
    public void execute() {
        book.setWorkingSheet(sheet);
        oldRange = book.getRange(sheet, name);
        book.setRange(name, range);
    }

    @Override
    public void undo() {
        book.setWorkingSheet(sheet);
        if (oldRange.equals("")) {
            book.removeRange(name);
        } else {
            book.setRange(name, oldRange);
        }
    }

}
