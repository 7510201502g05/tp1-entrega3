package ar.fiuba.tdd.tp1.commands;

import ar.fiuba.tdd.tp1.model.Book;

import java.util.Collection;

/**
 * Created by ale on 10/5/15.
 */
public class AddNewBookCommand implements Command {

    private final Collection<Book> books;
    private Book book;

    /**
     * Constructor de AddNewBookCommand.
     *
     * @param books    the collection of books
     * @param bookName the name of the book to create and add to @books
     */
    public AddNewBookCommand(Collection<Book> books, String bookName) {
        this.books = books;
        this.book = new Book(bookName);
    }

    @Override
    public void execute() {
        books.add(this.book);
    }

    @Override
    public void undo() {
        books.remove(this.book);
    }
}
