package ar.fiuba.tdd.tp1.commands;

import ar.fiuba.tdd.tp1.model.Book;

/**
 * Esta clase se encarga de deshacer y rehacer la eliminacion de un rango en una hoja. 
 * 
 * @author Galli
 *
 */
public class SheetRemoveRangeCommand implements Command {

    private Book book;
    private String name;
    private String oldRange;
    private String sheet;

    public SheetRemoveRangeCommand(Book book, String sheet, String name) {
        this.name = name;
        this.sheet = sheet;
        this.book = book;
        this.oldRange = "";
    }

    @Override
    public void execute() {
        book.setWorkingSheet(sheet);
        oldRange = book.getRange(sheet, name);
        book.removeRange(name);
    }

    @Override
    public void undo() {
        book.setWorkingSheet(sheet);
        if (!oldRange.equals("")) {
            book.setRange(name, oldRange);
        }
    }

}
