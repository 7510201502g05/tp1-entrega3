package ar.fiuba.tdd.tp1.persistence;

import ar.fiuba.tdd.tp1.exceptions.NoBookException;
import ar.fiuba.tdd.tp1.exceptions.NoSheetsException;
import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.SpreadSheetApplication;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.List;

/**
 * Created by ale on 10/29/15.
 */
public class CsvParser {

    public void dumpCSV(SpreadSheetApplication app, String book, String sheet, String fileName) {
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator("\n");

        //Writer fileWriter = null;
        //CSVPrinter csvFilePrinter = null;

        try {
            //initialize FileWriter object
            Writer fileWriter = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(fileName), "utf-8"));
            //initialize CSVPrinter object
            CSVPrinter csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
            //Create CSV file header
            //csvFilePrinter.printRecord(sheet);
            //Write a row to the CSV file
            int maxRows = app.getMaxRowsOfSheet(book, sheet);
            //System.out.println("MaxRows= " + maxRows);
            for (int row = 1; row <= maxRows; row++) {
                List<String> rowData = app.getRowValuesOf(book, sheet, row);
                csvFilePrinter.printRecord(rowData);
            }
            fileWriter.flush();
            fileWriter.close();
            csvFilePrinter.close();

        } catch (NoBookException e) {
            e.printStackTrace();
        } catch (NoSheetsException e) {
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Error in FileWriter !!!");
            e.printStackTrace();
        } /*finally {
            try {
                fileWriter.close();
                csvFilePrinter.close();
            } catch (IOException e) {
                System.out.println("Error while closing fileReader/csvFileParser !!!");
                e.printStackTrace();
            }
        }*/
    }


    public void loadCSV(SpreadSheetApplication app, String book, String sheet, String fileName) {
        Book newBook = new Book(book);
        newBook.addSheet(sheet);
        app.setRetrievedBook(newBook);

        Reader fileReader = null;

        CSVParser csvFileParser = null;

        //Create the CSVFormat object with the header mapping
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withAllowMissingColumnNames();

        try {

            //initialize FileReader object
            fileReader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(fileName), "utf-8"));

            //initialize CSVParser object
            csvFileParser = new CSVParser(fileReader, csvFileFormat);

            //Get a list of CSV file records
            List<CSVRecord> csvRecords = csvFileParser.getRecords();

            //Read the CSV file records starting from the second record to skip the header
            for (int row = 0; row < csvRecords.size(); row++) {
                CSVRecord rowData = csvRecords.get(row);
                for (char col = 'A'; col < 'A' + rowData.size(); col++) {
                    String cellName = String.valueOf(col) + String.valueOf(row + 1);
                    app.setFormulaOf(book, sheet, cellName, rowData.get(col - 'A'));
                    //System.out.println(cellName + " : " + rowData.get(col-'A'));
                }
            }

        } catch (Exception e) {
            System.out.println("Error in CsvFileReader !!!");
            e.printStackTrace();
        } /*finally {
            try {
                fileReader.close();
                csvFileParser.close();
            } catch (IOException e) {
                System.out.println("Error while closing fileReader/csvFileParser !!!");
                e.printStackTrace();
            }
        }*/
    }
}
