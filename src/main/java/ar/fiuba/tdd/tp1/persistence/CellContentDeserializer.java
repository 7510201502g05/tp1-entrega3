package ar.fiuba.tdd.tp1.persistence;

import com.google.gson.*;

import ar.fiuba.tdd.tp1.model.formulas.CellContent;


import java.lang.reflect.Type;

/**
 * Created by ale on 10/25/15.
 */
public class CellContentDeserializer implements JsonDeserializer<CellContent> {
    @Override
    public CellContent deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jobject = (JsonObject) json;

        JsonElement formulaRepresentation = jobject.get("formulaRepresentation");

        JsonArray formatsOfContent = jobject.getAsJsonArray("formatsOfContent");

        CellContent content = new CellContent();

        /*Seteo la formula representation pero sin parsearla porque pueden haber referencias a otras celdas que todavia
        * no fueron creadas*/
        content.setFormulaRepresentation(formulaRepresentation.getAsString());

        /*Paso los formatters de JasonArrays a String[] y los seteo*/
        for (JsonElement formatOptions : formatsOfContent) {
            JsonArray formatOptionsArray = formatOptions.getAsJsonArray();
            String[] options = new String[formatOptionsArray.size()];
            for (int i = 0; i < formatOptionsArray.size(); i++) {
                options[i] = formatOptionsArray.get(i).getAsString();
            }
            content.setFormatter(options);
        }

        JsonElement contentType = jobject.get("contentType");

        content.setContentType(contentType.getAsString());

        return content;
    }
}