package ar.fiuba.tdd.tp1.persistence;

import com.google.gson.*;

import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.HashMappedSheet;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ale on 10/25/15.
 */
public class BookDeserializer implements JsonDeserializer<Book> {
    @Override
    public Book deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jobject = (JsonObject) json;

        JsonElement bookName = jobject.get("bookName");

        Book book = new Book(bookName.getAsString());

        JsonArray jsonSheets = jobject.getAsJsonArray("sheets");

        List<HashMappedSheet> sheets = new ArrayList<>();

        for (JsonElement jsonSheet : jsonSheets) {
            HashMappedSheet sheet = context.deserialize(jsonSheet, HashMappedSheet.class);
            sheets.add(sheet);
        }

        book.setSheets(sheets);

        book.updateCellsContent();

        return book;
    }
}
