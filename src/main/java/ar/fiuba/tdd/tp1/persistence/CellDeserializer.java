/*package ar.fiuba.tdd.tp1.persistence;

import ar.fiuba.tdd.tp1.model.Cell;
import com.google.gson.*;

import java.lang.reflect.Type;
*/
/**
 * Created by ale on 10/22/15.
 * Ejemplo de un CUSTOM DESERIALIZER.
 *
class CellDeserializer implements JsonDeserializer<Cell> {
    public Cell deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {

        System.out.println("\n\nDESSERIALIZANDO CELL!!!\n");

        JsonObject jobject = (JsonObject) json;

        JsonElement name = jobject.get("name");

        Cell theCell = new Cell(name.getAsString());

        JsonObject content = jobject.get("content").getAsJsonObject();

        JsonElement formulaRepresentation = content.get("formulaRepresentation");

        JsonArray formats = content.getAsJsonArray("formatsOfContent");

//        theCell

        System.out.println("\n\nIMPRIMO NAME\n");
        System.out.println(name.getAsString());
        return theCell;
    }
}
 */